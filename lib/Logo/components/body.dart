import 'package:edu_app/login/welcome_screen.dart';
import 'package:flutter/material.dart';


class Body extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) {
                    return WelcomeScreen();
                  },
                ),
              );
            },
            child: Container(
              width: 1440.0,
              height: 2560.0,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment(-1.0, -1.0),
                  end: Alignment(1.0, -0.85),
                  colors: [const Color(0xff145d7c), const Color(0xff33c0ba)],
                  stops: [0.0, 1.0],
                ),
                image: DecorationImage(
                  image: const AssetImage('assets/dr.png'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
