import 'package:flutter/material.dart';

class Background extends StatelessWidget {
  final Widget child;
  const Background({
    Key key,
    @required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xffffffff),
      body: Stack(
        children: <Widget>[
          Container(
            width: 1440.0,
            height: 2560.0,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment(-1.0, -1.0),
                end: Alignment(1.0, -0.85),
                colors: [const Color(0xff145d7c), const Color(0xff33c0ba)],
                stops: [0.0, 1.0],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
