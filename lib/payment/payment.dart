//import 'package::edu_app/pages/paysucss.dart';
import 'package:edu_app/Home/home.dart';
import 'package:edu_app/payment/paymentfailed.dart';
import 'package:edu_app/payment/paysucss.dart';
import 'package:edu_app/viewCourses/subcourse.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class Payment extends StatefulWidget {
  @override
  _PaymentState createState() => _PaymentState();
}

class _PaymentState extends State<Payment> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        child: CustomAppBar(title: 'Payment Method'),
        preferredSize: Size.fromHeight(60.0),
      ),
      body: Container(
        color: Colors.white54,
        child: ListView(children: [
          SizedBox(
            height: 20,
          ),
          // ignore: missing_required_param

          // ignore: missing_required_param
          MenuItem(
            text1: '     9745677567',
            text2: '₹100',
            press: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => (PaymentSuccessfull())));
            },
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20, top: 20, bottom: 05),
            child: Text(
              'Cards',
              style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          // ignore: missing_required_param
          MenuItem(
            icon: Icon(
              FontAwesomeIcons.creditCard,
              color: Colors.indigo,
            ),
            text1: 'Debit/Credit Cards',
            press: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => PaymentFailed()));
            },
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20, top: 20, bottom: 05),
            child: Text(
              'UPI',
              style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          // ignore: missing_required_param
          MenuItem(
            icon: Icon(
              FontAwesomeIcons.googlePay,
              color: Colors.indigo,
            ),
            text1: 'GooglePay/PhonePay',
            press: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => Subcourse()));
            },
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20, top: 20, bottom: 05),
            child: Text(
              'Wallets',
              style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          // ignore: missing_required_param
          MenuItem(
            icon: Icon(
              FontAwesomeIcons.paypal,
              color: Colors.indigo,
            ),
            text1: 'Paytm',
            text2: '',
            press: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => Subcourse()));
            },
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20, top: 20, bottom: 05),
            child: Text(
              'Net Banking',
              style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          // ignore: missing_required_param
          MenuItem(
            icon: Icon(
              FontAwesomeIcons.piggyBank,
              color: Colors.indigo,
            ),

            text1: 'Net Banking',
            text2: '',
            // imageUrl: 'assets/10.jpg',
            press: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => Subcourse()));
            },
          ),
          SizedBox(
            height: 20,
          ),
          // ignore: missing_required_param
          MenuItem(
            text1: 'Pay Later',
            text2: '',
            press: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => Subcourse()));
            },
          ),
          SizedBox(
            height: 20,
          ),
        ]),
      ),
      bottomNavigationBar: Container(
        padding: EdgeInsets.symmetric(vertical: 14),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(40),
            topRight: Radius.circular(40),
          ),
          boxShadow: [
            BoxShadow(
              offset: Offset(0, -15),
              blurRadius: 20,
              color: Color(0xFFDADADA).withOpacity(0.15),
            ),
          ],
        ),
        child: SafeArea(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(
                width: 0.01,
              ),
              IconButton(icon: Icon(Icons.home), onPressed: () {}),
              IconButton(icon: Icon(Icons.bookmark), onPressed: () {}),
              IconButton(icon: Icon(Icons.notifications), onPressed: () {}),
              IconButton(icon: Icon(Icons.play_arrow), onPressed: () {}),
              SizedBox(
                width: 0.01,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class MenuItem extends StatelessWidget {
  const MenuItem(
      {Key key,
      @required this.text1,
      @required this.icon,
      @required this.press,
      this.text2,
      // this.imageUrl,
      ListTile child})
      : super(key: key);
  // final String imageUrl;
  final String text1, text2;
  final Widget icon;
  final VoidCallback press;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey[200],
      height: 50.0,
      child: Padding(
        padding: const EdgeInsets.only(left: 05, right: 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                // imageUrl != null
                //     ? ImageIcon(
                //         AssetImage(imageUrl),
                //         color: null,
                //         size: 100.0,
                //       )
                //     : Text(''),
                // ignore: missing_required_param
                icon != null
                    ? IconButton(
                        icon: icon,
                        onPressed: () {},
                        iconSize: 22.0,
                      )
                    : Text(''),
                Text(
                  text1,
                  style:
                      TextStyle(height: 1, fontSize: 20, color: Colors.black),
                ),
              ],
            ),
            GestureDetector(
              onTap: press,
              child: Icon(Icons.arrow_forward_ios),
              // child: Text(
              //   text2,
              //   style: TextStyle(height: 1, fontSize: 20, color: Colors.black),
              // ),
            ),
            // imageUrl != null
            //     ? ImageIcon(
            //         AssetImage(imageUrl),
            //         size: 150.0,
            //       )
            //     : Text('')
          ],
        ),
      ),
    );
  }
}
