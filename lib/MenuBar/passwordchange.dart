
import 'package:edu_app/login/RoundedPasswordChangeField.dart';
import 'package:edu_app/login/components/background.dart';
import 'package:edu_app/login/rounded_button.dart';
import 'package:edu_app/login/rounded_password_field.dart';
import 'package:edu_app/login/welcome_screen.dart';
import 'package:flutter/material.dart';

//import 'package:edu_app/Screens/home/homepage.dart';


class Passwordchange extends StatefulWidget {
  const Passwordchange({
    Key key,
  }) : super(key: key);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Passwordchange> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
            size: 25.0,
          ),
          onPressed: () => Navigator.pop(context),
        ),
        backgroundColor: Color(0xff33c0ba),
        title: Text(
          "Settings",
          style: TextStyle(fontSize: 20.0, color: Colors.white),
        ),
      ),
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(height: size.height * 0.30),
            RoundedPasswordField(
              hintText: "Entering Password",
              onChanged: (value) {},
            ),
            RoundedPasswordChangeField(
              hintText: "New Password",
              onChanged: (value) {},
            ),
            RoundedPasswordField(
              hintText: "Confirm your Password",
              onChanged: (value) {},
            ),
            SizedBox(height: size.height * 0.08),
            RoundedButton(
              text: "Change",
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return WelcomeScreen();
                    },
                  ),
                );
              },
            ),
            RoundedButton(
              text: "Reset",
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return WelcomeScreen();
                    },
                  ),
                );
              },
            ),
            SizedBox(height: size.height * 0.03),
          ],
        ),
      ),
    );
  }
}
