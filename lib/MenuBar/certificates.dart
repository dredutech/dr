import 'package:flutter/material.dart';

class Certificates extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
            size: 25.0,
          ),
          onPressed: () => Navigator.pop(context),
        ),
        backgroundColor: Color(0xff33c0ba),
        title: Text(
          "Certificates",
          style: TextStyle(fontSize: 24.0, color: Colors.white),
        ),
      ),
      body: ListView(
        children: [
          SizedBox(
            height: 50.0,
          ),
          GestureDetector(
            //  onTap: () => Navigator.push(
            // context, MaterialPageRoute(builder: (context) => FullVideo())),
            child: Stack(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: <Widget>[
                      Container(
                        height: 200.0,
                        decoration: BoxDecoration(
                          //  color: Colors.red,
                          image: DecorationImage(
                              image: AssetImage(
                                "assets/4.jpg",
                              ),
                              fit: BoxFit.cover),

                          borderRadius: BorderRadius.circular(10),

                          boxShadow: <BoxShadow>[
                            BoxShadow(
                                //color: Color(0xff33c0ba),
                                //blurRadius: 60.0,
                                offset: Offset(0.95, 0.95))
                          ],
                          // image: DecorationImage(
                          //    Image.(Icons.assessment),
                          //   fit: BoxFit.cover,
                          // )
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Text(
                          "Digital marketing ",
                          //  style: titleTextStyle,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16.0),
                        child: Row(
                          children: <Widget>[],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 30.0,
          ),
          GestureDetector(
            // onTap: () => Navigator.push(
            //     context, MaterialPageRoute(builder: (context) => FullVideo())),
            child: Stack(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: <Widget>[
                      Container(
                        height: 200.0,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(
                                "assets/4.jpg",
                              ),
                              fit: BoxFit.cover),
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                                //  color: Color(0xff33c0ba),
                                //  blurRadius: 60.0,
                                offset: Offset(0.95, 0.95))
                          ],
                          // image: DecorationImage(
                          //    Image.(Icons.assessment),
                          //   fit: BoxFit.cover,
                          // )
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Text(
                          "Webdevelopment",
                          //  style: titleTextStyle,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16.0),
                        child: Row(
                          children: <Widget>[],
                        ),
                      ),
                      const SizedBox(height: 20.0),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 10.0,
          ),
          GestureDetector(
            // onTap: () => Navigator.push(
            //   context, MaterialPageRoute(builder: (context) => FullVideo())),
            child: Stack(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: <Widget>[
                      Container(
                        height: 200.0,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(
                                "assets/4.jpg",
                              ),
                              fit: BoxFit.cover),
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                                // color: Color(0xff33c0ba),
                                //  blurRadius: 60.0,
                                offset: Offset(0.95, 0.95))
                          ],
                          // image: DecorationImage(
                          //    Image.(Icons.assessment),
                          //   fit: BoxFit.cover,
                          // )
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Text(
                          "Marketing",
                          //  style: titleTextStyle,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16.0),
                        child: Row(
                          children: <Widget>[],
                        ),
                      ),
                      const SizedBox(height: 20.0),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
