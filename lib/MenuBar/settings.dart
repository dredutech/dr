import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
//import 'package:flutter_auth/components/rounded_button.dart';
//import 'package:flutter_auth/components/rounded_input_field.dart';

class SettingsTwoPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
            size: 25.0,
          ),
          onPressed: () => Navigator.pop(context),
        ),
        backgroundColor: Color(0xff33c0ba),
        title: Text(
          "Settings",
          style: TextStyle(fontSize: 20.0, color: Colors.white),
        ),
      ),
      body: Theme(
        data: Theme.of(context).copyWith(
          brightness: Brightness.dark,
          primaryColor: Color(0xff33c0ba),
        ),
        child: DefaultTextStyle(
          style: TextStyle(
            color: Color(0xff33c0ba),
          ),
          child: SingleChildScrollView(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              children: <Widget>[
                const SizedBox(height: 30.0),
                // Row(
                //   children: <Widget>[
                //     // Container(
                //     //   width: 25,
                //     //   height: 25,
                //     // ),
                //   ],
                // ),
                // const SizedBox(height: 5.0),
                ListTile(
                  subtitle: Text(
                    "Terms & Conditions",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  trailing: Icon(
                    Icons.keyboard_arrow_right,
                    color: Color(0xff33c0ba),
                  ),
                  onTap: () {},
                ),
                ListTile(
                  subtitle: Text(
                    "Privacy Policy",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  trailing: Icon(
                    Icons.keyboard_arrow_right,
                    color: Color(0xff33c0ba),
                  ),
                  onTap: () {},
                ),
                ListTile(
                  subtitle: Text(
                    "Help & Support",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  trailing: Icon(
                    Icons.keyboard_arrow_right,
                    color: Color(0xff33c0ba),
                  ),
                  onTap: () {},
                ),
                ListTile(
                  subtitle: Text(
                    "Language",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  trailing: Icon(
                    Icons.keyboard_arrow_right,
                    color: Color(0xff33c0ba),
                  ),
                  onTap: () {},
                ),
                ListTile(
                  subtitle: Text(
                    "Security",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  trailing: Icon(
                    Icons.keyboard_arrow_right,
                    color: Color(0xff33c0ba),
                  ),
                  onTap: () {},
                ),
                ListTile(
                  subtitle: Text(
                    "Theme",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  trailing: Icon(
                    Icons.keyboard_arrow_right,
                    color: Color(0xff33c0ba),
                  ),
                  onTap: () {},
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
