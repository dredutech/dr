import 'package:edu_app/MenuBar/passwordchange.dart';
import 'package:edu_app/card/creditcard.dart';
import 'package:edu_app/viewCourses/InstructorProfile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
//import 'package:flutter_auth/Screens/Welcome/welcome_screen.dart';
//import '../card/creditcard.dart';
//import 'package:edu_app/Screens/home/homepage.dart';
//import 'package:edu_app/Screens/home/passwordchange.dart';
//import '../../components/Screens/home/passwordchange.dart';
//import 'package:flutter_auth/components/rounded_button.dart';

class ProfileEightPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // colors: [const Color(0xff145d7c), const Color(0xff33c0ba)],
        //backgroundColor: const Color(0xff145d7c,0xff33c0ba),
        // backgroundColor: const Color(0xff33c0ba),
        extendBodyBehindAppBar: true,
        extendBody: true,
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Colors.white,
              size: 25.0,
            ),
            onPressed: () => Navigator.pop(context),
          ),
          backgroundColor: Color(0xff33c0ba),
          title: Text(
            "My Profile",
            style: TextStyle(fontSize: 20.0, color: Colors.white),
          ),
          // backgroundColor: Colors.transparent,
          //  elevation: 0,
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              const SizedBox(height: 25.0),
              ProfileHeader(
                avatar: NetworkImage('assets/dr.png'),
                coverImage: NetworkImage('assets/dr.png'),
                title: "ABHI RAM",
                subtitle: "Manager",
                actions: <Widget>[
                  MaterialButton(
                    color: Colors.white,
                    shape: CircleBorder(),
                    elevation: 0,
                    child: Icon(Icons.edit),
                    onPressed: () {},
                  )
                ],
              ),
              const SizedBox(height: 55.0),
              UserInfo(),
            ],
          ),
        ));
  }
}

class UserInfo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      child: Column(
        // children: <Widget>[
        //   Card(
        children: [
          Container(
            // decoration: BoxDecoration(
            //   borderRadius: BorderRadius.circular(20.0),
            //   gradient: LinearGradient(
            //     begin: Alignment.topCenter,
            //     end: Alignment.bottomCenter,
            //     colors: [
            //       const Color(0xff33c0ba),
            //       const Color(0xff145d7c),
            //     ],
            //     stops: [0.0, 1.0],
            //   ),
            // ),
            alignment: Alignment.topLeft,
            padding: EdgeInsets.all(50),
            child: Column(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    ListTile(
                      leading: Icon(Icons.email),
                      title: Text("Email"),
                      subtitle: Text("varsha@goyalo.com"),
                    ),
                    ListTile(
                      leading: Icon(Icons.phone),
                      title: Text("Phone"),
                      subtitle: Text("9945342312"),
                    ),
                    ListTile(
                      leading: Icon(Icons.lock),
                      title: Text("Password Change"),
                      subtitle: Text("9945342312"),
                      trailing: Icon(Icons.keyboard_arrow_right),
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => Passwordchange(),
                          ),
                        );
                      },
                    ),
                    ListTile(
                      leading: Icon(Icons.album),
                      title: Text("Card Details"),
                      subtitle: Text("9945342312"),
                      trailing: Icon(Icons.keyboard_arrow_right),
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => Creditcard(),
                          ),
                        );
                      },
                    ),
                  ],
                )
              ],
            ),
          ),
        ],
        // )
        //],
      ),
    );
  }
}
