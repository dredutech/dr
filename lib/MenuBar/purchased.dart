import 'package:flutter/material.dart';

//import 'package:flutter_auth/constants.dart';
class PurchasedVideo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
            size: 25.0,
          ),
          onPressed: () => Navigator.pop(context),
        ),
        backgroundColor: Color(0xff33c0ba),
        title: Text(
          "Purchased Videos",
          style: TextStyle(fontSize: 20.0, color: Colors.white),
        ),
      ),
      body: ListView(children: [
        SizedBox(
          height: 30.0,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // Text(
            //   "Purchased videos",
            //   style: TextStyle(fontSize: 22.0),
            // ),
            SizedBox(
              height: 30.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage(
                            "assets/4.jpg",
                          ),
                          fit: BoxFit.cover),
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                            //color: Color(0xff33c0ba),
                            //blurRadius: 60.0,
                            offset: Offset(0.95, 0.95))
                      ],
                      color: Colors.grey),
                  width: 150.0,
                  height: 100,
                ),
                Column(
                  children: [
                    Text(
                      "Digital Maketing ",
                      style: TextStyle(fontSize: 20.0),
                    ),
                    Text(
                      "SEO",
                      style: TextStyle(fontSize: 18.0),
                    ),
                  ],
                ),
              ],
            ),
            SizedBox(
              height: 30.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage(
                            "assets/4.jpg",
                          ),
                          fit: BoxFit.cover),
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                            // color: Color(0xff33c0ba),
                            // blurRadius: 60.0,
                            offset: Offset(0.95, 0.95))
                      ],
                      color: Colors.grey),
                  width: 150.0,
                  height: 100,
                ),
                Column(
                  children: [
                    Text(
                      "Digital Maketing ",
                      style: TextStyle(fontSize: 20.0),
                    ),
                    Text(
                      "SEO",
                      style: TextStyle(fontSize: 18.0),
                    ),
                  ],
                ),
              ],
            ),
            SizedBox(
              height: 30.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage(
                            "assets/4.jpg",
                          ),
                          fit: BoxFit.cover),
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                            // color: Color(0xff33c0ba),
                            // blurRadius: 60.0,
                            offset: Offset(0.95, 0.95))
                      ],
                      color: Colors.grey),
                  width: 150.0,
                  height: 100,
                ),
                Column(
                  children: [
                    Text(
                      "Digital Maketing ",
                      style: TextStyle(fontSize: 20.0),
                    ),
                    Text(
                      "SEO",
                      style: TextStyle(fontSize: 18.0),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
        SizedBox(
          height: 30.0,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Container(
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage(
                        "assets/4.jpg",
                      ),
                      fit: BoxFit.cover),
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                        //color: Color(0xff33c0ba),
                        // blurRadius: 60.0,
                        offset: Offset(0.95, 0.95))
                  ],
                  color: Colors.grey),
              width: 150.0,
              height: 100,
            ),
            Column(
              children: [
                Text(
                  "Digital Maketing ",
                  style: TextStyle(fontSize: 20.0),
                ),
              ],
            ),
          ],
        ),
        SizedBox(
          height: 30.0,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Container(
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage(
                        "assets/4.jpg",
                      ),
                      fit: BoxFit.cover),
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                        // color: Color(0xff33c0ba),
                        //blurRadius: 60.0,
                        offset: Offset(0.95, 0.95))
                  ],
                  color: Colors.grey),
              width: 150.0,
              height: 100,
            ),
            Column(
              children: [
                Text(
                  "Digital Maketing ",
                  style: TextStyle(fontSize: 20.0),
                ),
              ],
            ),
          ],
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: 14,
            ),
            SizedBox(
              height: 30.0,
            ),
          ],
        ),
      ]),
    );
  }
}
