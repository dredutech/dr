
import 'package:edu_app/Home/home.dart';
import 'package:edu_app/viewCourses/VideoProfile.dart';
import 'package:edu_app/viewCourses/subcourse.dart';
import 'package:flutter/material.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
//import 'package:edu_app/pages/smooth_star_rating.dart';

class Webinar extends StatefulWidget {
  @override
  _WebinarState createState() => _WebinarState();
}

class _WebinarState extends State<Webinar> {
  @override
  Widget build(BuildContext context) {
    var rating = 3.0;
    return Scaffold(
      appBar: PreferredSize(
        child: CustomAppBar(title: 'Webinar'),
        preferredSize: Size.fromHeight(60.0),
      ),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "",
                  style: TextStyle(fontSize: 22.0),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(
                                "assets/4.jpg",
                              ),
                              fit: BoxFit.cover),
                          borderRadius: BorderRadius.circular(20),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                                color: Colors.grey,
                                blurRadius: 5.0,
                                offset: Offset(0.75, 0.95))
                          ],
                          color: Colors.grey),
                      width: 380.0,
                      height: 250,
                    ),
                    SizedBox(
                      height: 30.0,
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "Digital Maketing -part1 search engine optimization",
                    style: TextStyle(fontSize: 20),
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      SmoothStarRating(
                          allowHalfRating: false,
                          onRated: (v) {},
                          starCount: 5,
                          rating: rating,
                          size: 30.0,
                          isReadOnly: true,
                          filledIconData: Icons.star,
                          halfFilledIconData: Icons.star_half,
                          defaultIconData: Icons.star_border,
                          color: Colors.green,
                          borderColor: Colors.green,
                          spacing: 0.0),
                      Text('4.0k views'),
                    ],
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15.0),
                  child: Column(
                    children: <Widget>[
                      ExpansionTile(
                        title: Text(
                          "Description",
                          style: TextStyle(
                              fontSize: 18.0, fontWeight: FontWeight.bold),
                        ),
                        children: <Widget>[
                          ListTile(
                            title: new Text(
                                'The entire course was completely re-recorded and updated - it is totally up-to-date with the latest version of Flutter!\n, With the latest update, I also added Push Notifications and Image Upload!\n, '),
                          ),
                        ],
                      ),
                      ExpansionTile(
                        title: Text(
                          "comments",
                          style: TextStyle(
                              fontSize: 18.0, fontWeight: FontWeight.bold),
                        ),
                        children: <Widget>[
                          // ListTile(
                          //   title: new Text(
                          //       'The entire course was completely re-recorded and updated - it is totally up-to-date with the latest version of Flutter!\n, With the latest update, I also added Push Notifications and Image Upload!\n, '),
                          // ),
                          SizedBox(
                            height: 20.0,
                          ),

                          ProfileMenu(
                            avatar: NetworkImage('assets/5.jpg'),
                            text: 'Ecommerce SEO',
                            press: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => VideoProfile()));
                            },
                          ),

                          // ignore: missing_required_param
                          SizedBox(
                            height: 20.0,
                          ),
                          ProfileMenu(
                            text:
                                'PPC gv ug gvjhb AdvertisemenhyugS JHVikd GK BDUJd UJKB N UJK tjhzikfbkbibkibfkjaoiufbikbiukajbfiukhbiuhiubakjfkhvbkkkkkkkkkkkkkfvzkshdfj',
                            press: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Webinar()));
                            },
                          ),
                          // ignore: missing_required_param
                          SizedBox(
                            height: 20.0,
                          ),
                          ProfileMenu(
                              text: 'Lead Generation',
                              press: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => Subcourse()));
                              }),
                          SizedBox(
                            height: 20.0,
                          ),
                          // ignore: missing_requi
                        ],
                      ),
                    ],
                  ),
                ),

                // Padding(
                //   padding: const EdgeInsets.all(8.0),
                //   child: Text(
                //     "Digital Maketing -part1 search engine optimaization",
                //     style: TextStyle(fontSize: 20),
                //   ),
                // ),
                // SizedBox(
                //   height: 30.0,
                // ),
                // Padding(
                //   padding: const EdgeInsets.symmetric(horizontal: 30.0),
                //   child: Column(
                //     mainAxisAlignment: MainAxisAlignment.center,
                //     children: <Widget>[
                //       Row(
                //         children: <Widget>[
                //           Expanded(
                //             child: RaisedButton(
                //               onPressed: () {
                //                 Navigator.push(
                //                     context,
                //                     MaterialPageRoute(
                //                         builder: (BuildContext context) =>
                //                             Expansiontile()));
                //               },
                //               child: Text('ExpansionTile'),
                //             ),
                //           ),
                //         ],
                //       ),
                //     ],
                //   ),
                // ),
              ],
            ),
          ),
        ],
      ),
      bottomNavigationBar: Container(
        padding: EdgeInsets.symmetric(vertical: 14),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(40),
            topRight: Radius.circular(40),
          ),
          boxShadow: [
            BoxShadow(
              offset: Offset(0, -15),
              blurRadius: 20,
              color: Color(0xFFDADADA).withOpacity(0.15),
            ),
          ],
        ),
        child: SafeArea(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(
                width: 0.01,
              ),
              IconButton(icon: Icon(Icons.home), onPressed: () {}),
              IconButton(icon: Icon(Icons.bookmark), onPressed: () {}),
              IconButton(icon: Icon(Icons.notifications), onPressed: () {}),
              IconButton(icon: Icon(Icons.play_arrow), onPressed: () {}),
              SizedBox(
                width: 0.01,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class ProfileMenu extends StatelessWidget {
  const ProfileMenu({
    Key key,
    @required this.text,
    this.icon,
    @required this.press,
    NetworkImage avatar,
  }) : super(key: key);

  final String text, icon;
  final VoidCallback press;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 05),
      // child: GestureDetector(
      // onTap: press,

      child: Container(
        // height: 50.0,
        decoration: BoxDecoration(
            // border: Border.all(
            //   color: Colors.white,
            //   width: 15,
            // ),
            // borderRadius: BorderRadius.circular(30.0),
            // gradient: LinearGradient(
            //   begin: Alignment.topCenter,
            //   end: Alignment.bottomCenter,
            //   colors: [
            //     const Color(0xff33c0ba),
            //     const Color(0xff145d7c),
            //   ],
            //   stops: [0.0, 1.0],
            // ),
            ),

        child: Row(
          children: [
            Expanded(
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  text,
                  style:
                      TextStyle(height: 1, fontSize: 15, color: Colors.black),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class ProfileHeader extends StatelessWidget {
  final ImageProvider<dynamic> avatar;
  final String title;
  final String subtitle;

  final List<Widget> actions;

  const ProfileHeader(
      {Key key,
      @required this.avatar,
      @required this.title,
      this.subtitle,
      this.actions})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        actions != null
            ? Container(
                width: double.infinity,
                height: 10,
                padding: const EdgeInsets.only(bottom: 0.0, right: 0.0),
                alignment: Alignment.bottomRight,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: actions,
                ),
              )
            : Text(''),
        Container(
          width: double.infinity,
          margin: const EdgeInsets.only(top: 20),
          child: Column(children: <Widget>[
            Avatar(
              image: avatar,
              radius: 60,
              backgroundColor: Colors.white,
              borderColor: Colors.grey.shade300,
              borderWidth: 2.0,
            ),
            Text(
              title,
              // ignore: deprecated_member_use
              style: Theme.of(context).textTheme.title,
            ),
            subtitle != null
                ? Text(
                    subtitle,
                    // ignore: deprecated_member_use
                    style: Theme.of(context).textTheme.subtitle,
                  )
                : Text(''),
          ]),
        )
      ],
    );
  }
}

class Avatar extends StatelessWidget {
  final ImageProvider<dynamic> image;
  final Color borderColor;
  final Color backgroundColor;
  final double radius;
  final double borderWidth;

  const Avatar(
      {Key key,
      @required this.image,
      this.borderColor = Colors.grey,
      this.backgroundColor,
      this.radius = 50,
      this.borderWidth = 5})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      radius: radius + borderWidth,
      backgroundColor: borderColor,
      child: CircleAvatar(
        radius: radius,
        backgroundColor: backgroundColor != null
            ? backgroundColor
            : Theme.of(context).primaryColor,
        child: CircleAvatar(
          radius: radius - borderWidth,
          // backgroundImage: image,
        ),
      ),
    );
  }
}
