//import 'package:edu_app/pages/payment.dart';


import 'package:edu_app/Exam/exam/exam_screen.dart';
import 'package:edu_app/Home/AddToCart.dart';

import 'package:edu_app/payment/payment.dart';
import 'package:flutter/material.dart';

class VideoProfile extends StatefulWidget {
  @override
  _VideoProfileState createState() => _VideoProfileState();
}

class _VideoProfileState extends State<VideoProfile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.grey,
            size: 35.0,
          ),
          onPressed: () => Navigator.pop(context),
        ),
        backgroundColor: Colors.white,
        title: Text(
          "Course Profile",
          style: TextStyle(fontSize: 24.0, color: Colors.grey[800]),
        ),
      ),
      body: ListView(
        children: [
          SizedBox(
            height: 30.0,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Demo video",
                  style: TextStyle(fontSize: 22.0),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(
                                "assets/4.jpg",
                              ),
                              fit: BoxFit.cover),
                          borderRadius: BorderRadius.circular(20),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                                color: Colors.grey,
                                blurRadius: 5.0,
                                offset: Offset(0.75, 0.95))
                          ],
                          color: Colors.grey),
                      width: 380.0,
                      height: 250,
                    ),
                    SizedBox(
                      height: 30.0,
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "Digital Maketing -part1 search engine optimaization",
                    style: TextStyle(fontSize: 20),
                  ),
                ),
                SizedBox(
                  height: 30.0,
                ),
                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Row(
                    children: [
                      Text(
                        "₹100",
                        style: TextStyle(fontSize: 25, color: Colors.black),
                      ),
                    ],
                  ),
                ),
                // DescriptionTextWidget(
                //     text:
                //         ' Profile Video Specifications, 2 minutes longIncludes ,B  roll and interviewShot on location, 1600 includes scripting   '),
                // DescriptionTextWidget(
                //     text:
                //         'Instructor Name:Ajmal                                                                                                                          '),
                // DescriptionTextWidget(
                //     text:
                //         'What is Digital Marketing ? Digital marketing is the use of the Internet, mobile devices, social media, search engines, and other channels to reach consumers. Some marketing experts consider digital marketing to be an entirely new endeavor that requires a new way of approaching customers and new ways of understanding how customers behave compared to traditional marketing. ub, M8bjk,tmhbui'),
                // DescriptionTextWidget(
                //   text:
                //       '     Notes....... What is Digital Marketing ? Digital marketing is the use of the Internet, mobile devices, social media, search engines, and other channels to reach consumers. Some marketing experts consider digital marketing to be an entirely new endeavor that requires a new way of approaching customers and new ways of understanding how customers behave compared to traditional marketing. ub, M8bjk,tmhbui',
                // ),

                // Row(
                //   mainAxisSize: MainAxisSize.max,
                //   children: <Widget>[
                //     Expanded(
                //       child: MaterialButton(
                //         color: Color(0xff33c0ba),
                //         elevation: 1,
                //         onPressed: () {},
                //         child: Container(
                //           padding: EdgeInsets.all(15.0),
                //           child: Text(
                //             "Buy",
                //             textAlign: TextAlign.center,
                //             style: TextStyle(
                //                 fontSize: 20.0,
                //                 color: Colors.white,
                //                 fontWeight: FontWeight.w500),
                //           ),
                //         ),
                //       ),
                //     ),
                //     Expanded(
                //       child: MaterialButton(
                //         color: Color(0xff33c0ba),
                //         elevation: 1,
                //         onPressed: () {},
                //         child: Container(
                //           padding: EdgeInsets.all(15.0),
                //           child: Text(
                //             "Add to cart",
                //             textAlign: TextAlign.center,
                //             style: TextStyle(
                //                 fontSize: 20.0,
                //                 color: Colors.white,
                //                 fontWeight: FontWeight.w500),
                //           ),
                //         ),
                //       ),
                //     ),
                //  ],
                // ),
              ],
            ),
          ),
          ProfileMenu(
            text: 'Add to cart',
            press: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => CartOnePage()));
            },
          ),
          ProfileMenu(
            text: 'Pay Now',
            press: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Payment()));
            },
          ),
          ProfileMenu(
            text: ' Go to Exam',
            press: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => ExamScreen()));
            },
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15.0),
            child: Column(
              children: <Widget>[
                ExpansionTile(
                  title: Text(
                    "What you will learn",
                    style:
                        TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
                  ),
                  children: <Widget>[
                    ListTile(
                      title: new Text(
                          '• Learn Flutter and Dart from the ground up, step-by-step\n,• Use features like Google Maps, the device camera, authentication and much more!\n,• Learn all the basics without stopping after them: Dive deeply into Flutter & Dart and become an advanced developer\n, •Build engaging native mobile apps for both Android and iOS'),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15.0),
            child: Column(
              children: <Widget>[
                ExpansionTile(
                  title: Text(
                    "course content",
                    style:
                        TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
                  ),
                  children: <Widget>[
                    ExpansionTile(
                      title: new Text('Introduction'),
                      children: <Widget>[
                        ExpansionTile(
                          title: Text('What is flutter?'),
                          children: <Widget>[
                            ListTile(
                              title: Text(
                                  'Learning alone is absolutely fine but finding learning partners might be a nice thing, too. Our learning community is a great place to learn and grow together - of course it is 100% free and optional!'),
                            )
                          ],
                        )
                      ],
                    ),
                    ExpansionTile(
                      title: new Text('Understanding flutter architecture'),
                      children: <Widget>[
                        ListTile(
                          title: Text(
                              'We know what Flutter is, now let us take a look behind the scenes. Because it is pitch sounds a bit magic (create native cross-platform apps) - how does it work under the hood?'),
                        )
                      ],
                    ),
                    ExpansionTile(
                      title: new Text('Understanding flutter versions'),
                      children: <Widget>[
                        ListTile(
                          title: Text(
                              'Flutter is under active development, hence it is important to understand its versioning scheme and how it is kept up-to-date'),
                        )
                      ],
                    ),
                    ExpansionTile(
                      title: new Text('Course outline'),
                      children: <Widget>[
                        ListTile(
                          title: Text(
                              'We know what Flutter is about - in this lecture you will now find out what this course has to offer and how the content is structured.'),
                        )
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15.0),
            child: Column(
              children: <Widget>[
                ExpansionTile(
                  title: Text(
                    "Description",
                    style:
                        TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
                  ),
                  children: <Widget>[
                    ListTile(
                      title: new Text(
                          'The entire course was completely re-recorded and updated - it is totally up-to-date with the latest version of Flutter!\n, With the latest update, I also added Push Notifications and Image Upload!\n, '),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15.0),
            child: Column(
              children: <Widget>[
                ExpansionTile(
                  title: Text(
                    "Requirements",
                    style:
                        TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
                  ),
                  children: <Widget>[
                    ListTile(
                      title: new Text(
                          '• Basic programming language will help but is not a must-have\n,•You can use either Windows, macOS or Linux for Android app development - iOS apps can only be built on macOS though\n, • NO prior iOS or Android development experience is required\n, •NO prior Flutter or Dart experience is required - this course starts at zero!'),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              "Similar Videos",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
            ),
          ),
          SizedBox(
            height: 20.0,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage(
                            "assets/4.jpg",
                          ),
                          fit: BoxFit.cover),
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                            color: Colors.grey,
                            blurRadius: 5.0,
                            offset: Offset(0.75, 0.95))
                      ],
                      color: Colors.grey),
                  width: 150.0,
                  height: 100.0,
                ),
                Column(
                  children: [
                    Text(
                      "     Digital Maketing -part1 search",
                      style: TextStyle(fontSize: 15.0),
                    ),
                    Text(
                      "Freshers and job seekers",
                      style: TextStyle(fontSize: 15.0),
                    ),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(
            height: 05.0,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage(
                            "assets/4.jpg",
                          ),
                          fit: BoxFit.cover),
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                            color: Colors.grey,
                            blurRadius: 5.0,
                            offset: Offset(0.75, 0.95))
                      ],
                      color: Colors.grey),
                  width: 150.0,
                  height: 100.0,
                ),
                Column(
                  children: [
                    Text(
                      "     Digital Maketing -part1 search",
                      style: TextStyle(fontSize: 15.0),
                    ),
                    Text(
                      "Freshers and job seekers",
                      style: TextStyle(fontSize: 15.0),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
      bottomNavigationBar: Container(
        padding: EdgeInsets.symmetric(vertical: 14),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(40),
            topRight: Radius.circular(40),
          ),
          boxShadow: [
            BoxShadow(
              offset: Offset(0, -15),
              blurRadius: 20,
              color: Color(0xFFDADADA).withOpacity(0.15),
            ),
          ],
        ),
        child: SafeArea(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(
                width: 0.01,
              ),
              IconButton(icon: Icon(Icons.home), onPressed: () {}),
              IconButton(icon: Icon(Icons.bookmark), onPressed: () {}),
              IconButton(icon: Icon(Icons.notifications), onPressed: () {}),
              IconButton(icon: Icon(Icons.play_arrow), onPressed: () {}),
              SizedBox(
                width: 0.01,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class ProfileMenu extends StatelessWidget {
  const ProfileMenu({
    Key key,
    @required this.text,
    this.icon,
    @required this.press,
  }) : super(key: key);

  final String text, icon;
  final VoidCallback press;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 50, vertical: 10),
      child: GestureDetector(
        onTap: press,
        child: Container(
          height: 50.0,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30.0),
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                const Color(0xff33c0ba),
                const Color(0xff145d7c),
              ],
              stops: [0.0, 1.0],
            ),
          ),
          child: Row(
            children: [
              Expanded(
                child: Align(
                  alignment: Alignment.center,
                  child: Text(
                    text,
                    style:
                        TextStyle(height: 1, fontSize: 20, color: Colors.white),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
