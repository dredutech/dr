import 'package:edu_app/Logo/constants.dart';
import 'package:flutter/material.dart';

class RoundedButton extends StatelessWidget {
  final String text;
  final Function press;
  final Color color, textColor;
  const RoundedButton({
    Key key,
    this.text,
    this.press,
    this.color = kPrimaryColor,
    this.textColor = Colors.white,
    Text child,
    TextAlign textAlign,
    TextStyle style,
    List<Color> colors,
    Color backgroundColor,
    DecorationImage image,
    Text subtitle,
    Text title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 20),
      width: size.width * 0.5,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(29),
        //   decoration: BoxDecoration(
        //     borderRadius: BorderRadius.circular(30.0),
        //     gradient: LinearGradient(
        //       begin: Alignment.topCenter,
        //       end: Alignment.bottomCenter,
        //       colors: [
        //         const Color(0xff33c0ba),
        //         const Color(0xff145d7c),
        //       ],
        //       stops: [0.0, 1.0],
        //     ),
        //   ),
        // );
        // ignore: deprecated_member_use
        child: FlatButton(
          padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
          color: color,
          onPressed: press,
          child: Text(
            text,
            style: TextStyle(color: textColor),
          ),
        ),
      ),
    );
  }
}
