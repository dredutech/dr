import 'package:edu_app/Logo/constants.dart';
import 'package:flutter/material.dart';

class Forgetpasswordcheck extends StatelessWidget {
  final bool login;
  final Function press;
  const Forgetpasswordcheck({
    Key key,
    this.login = true,
    this.press,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        GestureDetector(
          onTap: press,
          child: Text(
            login ? "Forget Password ? " : "",
            style: TextStyle(
              color: kPrimaryColor,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        SizedBox(width: 30.0),
      ],
    );
  }
}
