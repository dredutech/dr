import 'package:edu_app/Logo/constants.dart';
import 'package:flutter/material.dart';

class Signup extends StatelessWidget {
  final bool login;
  final Function press;
  const Signup({
    Key key,
    this.login = true,
    this.press,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        // Text(
        //   login ? "Forget Password ? " : "Don’t have an Account ? ",
        //   textAlign: TextAlign.left,
        //   //style: TextStyle(color: kPrimaryColor),
        // ),
        GestureDetector(
          onTap: press,
          child: Text(
            login ? "Sign Up" : "Sign In",
            style: TextStyle(
              color: kPrimaryColor,
              fontWeight: FontWeight.bold,
            ),
          ),
        )
      ],
    );
  }
}
