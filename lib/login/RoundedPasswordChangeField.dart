import 'package:edu_app/Logo/constants.dart';
import 'package:edu_app/login/text_field_container.dart';
import 'package:flutter/material.dart';

class RoundedPasswordChangeField extends StatelessWidget {
  final ValueChanged<String> onChanged;
  const RoundedPasswordChangeField({
    Key key,
    this.onChanged,
    String hintText,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextField(
        obscureText: true,
        onChanged: onChanged,
        cursorColor: kPrimaryColor,
        decoration: InputDecoration(
          hintText: "Re-Enter Password",
          icon: Icon(
            Icons.lock,
            color: kPrimaryColor,
          ),
          suffixIcon: Icon(
            Icons.visibility,
            color: kPrimaryColor,
          ),
          border: InputBorder.none,
        ),
      ),
    );
  }
}
