//import 'package:edu_app/main.dart';
import 'package:edu_app/Email/email_screen.dart';
import 'package:edu_app/Home/main.dart';
import 'package:edu_app/Logo/components/background.dart';
import 'package:edu_app/Register/register_screen.dart';
import 'package:edu_app/login/forgetpassword.dart';
import 'package:edu_app/login/rounded_button.dart';
import 'package:edu_app/login/rounded_input_field.dart';
import 'package:edu_app/login/rounded_password_field.dart';
import 'package:edu_app/login/signup.dart';
import 'package:flutter/material.dart';
class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    // This size provide us total height and width of our screen
    var children2 = <Widget>[
      Text(
        "Welcome !",
        style: TextStyle(
          fontWeight: FontWeight.bold,
          // fontFamily: 'Open Sans',
          fontSize: 30,
          color: const Color(0xff000000),
          //fontWeight: FontWeight.w700,
        ),
        textAlign: TextAlign.left,
      ),
      Text(
        "Continue to Digirich edutech\ndigital university",
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
      // Text(
      //   "Digital University",
      //   style: TextStyle(fontWeight: FontWeight.bold),
      //   //textAlign: TextAlign.rightt,

      SizedBox(height: size.height * 0.05),
      RoundedInputField(
        hintText: " Enter your email or Phone number",
        onChanged: (value) {},
      ),
      RoundedPasswordField(
        hintText: " Enter your Password",
        onChanged: (value) {},
      ),
      Forgetpasswordcheck(
        press: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) {
                return EmailScreen();
              },
            ),
          );
        },
      ),
      SizedBox(height: size.height * 0.05),
      RoundedButton(
        backgroundColor: const Color(0xff33c0ba),
        text: "LOGIN",
        //  backgroundColor: const Color(0xff33c0ba),
        // colors: [const Color(0xff145d7c), const Color(0xff33c0ba)],
        press: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) {
              return MyHomePage();
            }),
          );
        },
      ),
      SizedBox(height: size.height * 0.01),
      Signup(
        press: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) {
                return RegisterScreen();
              },
            ),
          );
        },
      ),
    ];
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: children2,
        ),
      ),
    );
  }
}
