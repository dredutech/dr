import 'package:edu_app/Register/components/body.dart';
import 'package:edu_app/login/rounded_button.dart';
import 'package:flutter/material.dart';

final inputBorder = OutlineInputBorder(
  borderRadius: BorderRadius.circular(8.0),
  borderSide: BorderSide(color: Colors.grey.shade400),
);

final inputDecoration = InputDecoration(
  contentPadding: EdgeInsets.symmetric(vertical: 16.0),
  border: inputBorder,
  focusedBorder: inputBorder,
  enabledBorder: inputBorder,
);

class OTPPage extends StatelessWidget {
  // static final String path = "lib/src/pages/misc/otp.dart";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
    //  resizeToAvoidBottomPadding: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: Container(
        padding: const EdgeInsets.all(16.0),
        width: double.maxFinite,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SizedBox(height: 30.0),
            const SizedBox(height: 30.0),
            const SizedBox(height: 40.0),
            OTPFields(),
            const SizedBox(height: 40.0),
            Text(
              "Expiring in 02:22",
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 16.0, color: Colors.grey),
            ),
            const SizedBox(height: 10.0),
            TextButton(
              child: Text(
                "RESEND OTP",
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 18.0,
                  //fontWeight: FontWeight.w500,
                ),
              ),
              onPressed: () {},
            ),
            const SizedBox(height: 170.0),
            RoundedButton(
              backgroundColor: const Color(0xff33c0ba),
              text: "Confirm",
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return Body ();
                    },
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}

// class OTPFields extends StatefulWidget {
//   const OTPFields({
//     Key key,
//   }) : super(key: key);

//   @override
//   _OTPFieldsState createState() => _OTPFieldsState();
// }

// class _OTPFieldsState extends State<OTPFields> {
//   FocusNode pin2FN;
//   FocusNode pin3FN;
//   FocusNode pin4FN;
//   final pinStyle = TextStyle(fontSize: 32, fontWeight: FontWeight.bold);

//   @override
//   void initState() {
//     super.initState();
//     pin2FN = FocusNode();
//     pin3FN = FocusNode();
//     pin4FN = FocusNode();
//   }

//   @override
//   void dispose() {
//     super.dispose();
//     pin2FN?.dispose();
//     pin3FN?.dispose();
//     pin4FN?.dispose();
//   }

//   void nextField(String value, FocusNode focusNode) {
//     if (value.length == 1) {
//       focusNode.requestFocus();
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Form(
//       child: Column(
//         children: [
//           const SizedBox(height: 10.0),
//           Row(
//             mainAxisAlignment: MainAxisAlignment.spaceAround,
//             children: [
//               SizedBox(
//                 width: 40,
//                 child: TextFormField(
//                   autofocus: true,
//                   style: pinStyle,
//                   keyboardType: TextInputType.number,
//                   textAlign: TextAlign.center,
//                   decoration: inputDecoration,
//                   onChanged: (value) {
//                     nextField(value, pin2FN);
//                   },
//                 ),
//               ),
//               SizedBox(
//                 width: 40,
//                 child: TextFormField(
//                   autofocus: true,
//                   style: pinStyle,
//                   keyboardType: TextInputType.number,
//                   textAlign: TextAlign.center,
//                   decoration: inputDecoration,
//                   onChanged: (value) {
//                     nextField(value, pin2FN);
//                   },
//                 ),
//               ),
//               SizedBox(
//                 width: 40,
//                 child: TextFormField(
//                   autofocus: true,
//                   style: pinStyle,
//                   keyboardType: TextInputType.number,
//                   textAlign: TextAlign.center,
//                   decoration: inputDecoration,
//                   onChanged: (value) {
//                     nextField(value, pin2FN);
//                   },
//                 ),
//               ),
//               SizedBox(
//                 width: 40,
//                 child: TextFormField(
//                   focusNode: pin2FN,
//                   style: pinStyle,
//                   keyboardType: TextInputType.number,
//                   textAlign: TextAlign.center,
//                   decoration: inputDecoration,
//                   onChanged: (value) => nextField(value, pin3FN),
//                 ),
//               ),
//               SizedBox(
//                 width: 40,
//                 child: TextFormField(
//                   focusNode: pin3FN,
//                   style: pinStyle,
//                   keyboardType: TextInputType.number,
//                   textAlign: TextAlign.center,
//                   decoration: inputDecoration,
//                   onChanged: (value) => nextField(value, pin4FN),
//                 ),
//               ),
//               SizedBox(
//                 width: 40,
//                 child: TextFormField(
//                   focusNode: pin4FN,
//                   style: pinStyle,
//                   keyboardType: TextInputType.number,
//                   textAlign: TextAlign.center,
//                   decoration: inputDecoration,
//                   onChanged: (value) {
//                     if (value.length == 1) {
//                       pin4FN.unfocus();
//                     }
//                   },
//                 ),
//               ),
//             ],
//           ),
//           const SizedBox(height: 10.0),
//         ],
//       ),
//     );
//   }
// }
