//import 'package:edu_app/description.dart';
//import 'package:edu_app/Expansion.dart';
// import 'package:edu_app/Expansion.dart';
//import 'package:edu_app/pages/webinar.dart';
import 'package:flutter/material.dart';
//import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class FullVideo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.black,
            size: 35.0,
          ),
          onPressed: () => Navigator.pop(context),
        ),
        backgroundColor: Color(0xff33c0ba),
        title: Text(
          "Full Video",
          style: TextStyle(fontSize: 22.0, color: Colors.black),
        ),
      ),
      body: ListView(
        children: [
          SizedBox(
            height: 0.0,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // Text(
                //   "Full videos",
                //   style: TextStyle(fontSize: 22.0),
                // ),
                SizedBox(
                  height: 30.0,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage(
                                  "assets/4.jpg",
                                ),
                                fit: BoxFit.cover),
                            borderRadius: BorderRadius.circular(5),
                            boxShadow: <BoxShadow>[
                              BoxShadow(
                                  color: Color(0xff33c0ba),
                                  blurRadius: 65.0,
                                  offset: Offset(0.90, 0.90))
                            ],
                            color: Colors.grey),
                        width: 370.0,
                        height: 270.0,
                      ),
                      SizedBox(
                        height: 30.0,
                      ),
                    ],
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "Digital Marketing Search engine optimaization",
                    style: TextStyle(fontSize: 18.0),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      // Row(
                      //   children: <Widget>[
                      //     Expanded(
                      //       child: RaisedButton(
                      //         onPressed: () {
                      //           Navigator.push(
                      //               context,
                      //               MaterialPageRoute(
                      //                   builder: (BuildContext context) =>
                      //                       Expansiontile()));
                      //         },
                      //         child: Text('ExpansionTile'),
                      //       ),
                      //     ),
                      //   ],
                      // ),
                      // Row(
                      //   children: <Widget>[
                      //     Expanded(
                      //       child: RaisedButton(
                      //         onPressed: () {
                      //           Navigator.push(
                      //               context,
                      //               MaterialPageRoute(
                      //                   builder: (BuildContext context) =>
                      //                       Expansionpanel()));
                      //         },
                      //         child: Text('ExpansionPanel'),
                      //       ),
                      //     ),
                      //   ],
                      // )
                    ],
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30.0),
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 10.0),
                      ExpansionTile(
                        title: Text(
                          "What you will learn?",
                          style: TextStyle(
                              fontSize: 18.0, fontWeight: FontWeight.bold),
                        ),
                        children: <Widget>[
                          ExpansionTile(
                            title: Text(
                              ' Build a compete app for android and ios using the flutter with dart language',
                            ),
                            children: <Widget>[
                              ListTile(
                                title: Text(
                                    ' Design and build app using flutter , dart language with firebase '),
                              )
                            ],
                          ),
                          ListTile(
                            title: Text(
                                'Dart language and Google flutter , learn in simple way,by build full app using Google flutter , dart and firebase data base'),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30.0),
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 10.0),
                      ExpansionTile(
                        title: Text(
                          "Course content",
                          style: TextStyle(
                              fontSize: 18.0, fontWeight: FontWeight.bold),
                        ),
                        children: <Widget>[
                          ExpansionTile(
                            title: Text(
                              'Introduction',
                            ),
                            children: <Widget>[
                              ListTile(
                                title: Text(
                                    'lecture 3 build the contact page , between the different page inside the flutter'),
                              )
                            ],
                          ),
                          ListTile(
                            title: Text(
                                'lecture 4 build the authentication page with home screen'),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30.0),
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 10.0),
                      ExpansionTile(
                        title: Text(
                          "Sing up and Sign in design in the flutter",
                          style: TextStyle(
                              fontSize: 18.0, fontWeight: FontWeight.bold),
                        ),
                        children: <Widget>[
                          ExpansionTile(
                            title: Text(
                              'Sing up and Sign in design in the flutter part 1',
                            ),
                            children: <Widget>[
                              ListTile(
                                title: Text(
                                  'Sing up and Sign in design in the flutter part 2',
                                ),
                              )
                            ],
                          ),
                          ListTile(
                            title: Text(
                                'Sing up and Sign in design in the flutter part 3'),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30.0),
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 10.0),
                      ExpansionTile(
                        title: Text(
                          "Add the project to the firebase",
                          style: TextStyle(
                              fontSize: 18.0, fontWeight: FontWeight.bold),
                        ),
                        children: <Widget>[
                          ExpansionTile(
                            title: Text(
                              'Sing up and Sign in design in the flutter part 1',
                            ),
                            children: <Widget>[
                              ListTile(
                                title: Text(
                                  'Sing up and Sign in design in the flutter part 2',
                                ),
                              )
                            ],
                          ),
                          ListTile(
                            title: Text(
                                'Sing up and Sign in design in the flutter part 3'),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30.0),
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 10.0),
                      ExpansionTile(
                        title: Text(
                          "Add the project to the firebase",
                          style: TextStyle(
                              fontSize: 18.0, fontWeight: FontWeight.bold),
                        ),
                        children: <Widget>[
                          ExpansionTile(
                            title: Text(
                              'Sing up and Sign in design in the flutter part 1',
                            ),
                            children: <Widget>[
                              ListTile(
                                title: Text(
                                  'Sing up and Sign in design in the flutter part 2',
                                ),
                              )
                            ],
                          ),
                          ListTile(
                            title: Text(
                                'Sing up and Sign in design in the flutter part 3'),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30.0),
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 10.0),
                      ExpansionTile(
                        title: Text(
                          "Add the project to the firebase",
                          style: TextStyle(
                              fontSize: 18.0, fontWeight: FontWeight.bold),
                        ),
                        children: <Widget>[
                          ExpansionTile(
                            title: Text(
                              'Sing up and Sign in design in the flutter part 1',
                            ),
                            children: <Widget>[
                              ListTile(
                                title: Text(
                                  'Sing up and Sign in design in the flutter part 2',
                                ),
                              )
                            ],
                          ),
                          ListTile(
                            title: Text(
                                'Sing up and Sign in design in the flutter part 3'),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30.0),
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 10.0),
                      ExpansionTile(
                        title: Text(
                          "Add the project to the firebase",
                          style: TextStyle(
                              fontSize: 18.0, fontWeight: FontWeight.bold),
                        ),
                        children: <Widget>[
                          ExpansionTile(
                            title: Text(
                              'Sing up and Sign in design in the flutter part 1',
                            ),
                            children: <Widget>[
                              ListTile(
                                title: Text(
                                  'Sing up and Sign in design in the flutter part 2',
                                ),
                              )
                            ],
                          ),
                          ListTile(
                            title: Text(
                                'Sing up and Sign in design in the flutter part 3'),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30.0),
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 10.0),
                      ExpansionTile(
                        title: Text(
                          "Description",
                          style: TextStyle(
                              fontSize: 18.0, fontWeight: FontWeight.bold),
                        ),
                        children: <Widget>[
                          ExpansionTile(
                            title: Text(
                              'Sing up and Sign in design in the flutter part 1 sdsdsdsdsdsdsddfd',
                            ),
                            children: <Widget>[
                              ListTile(
                                title: Text(
                                  'Sing up and Sign in design in the flutter part 2',
                                ),
                              )
                            ],
                            // ),
                            // ListTile(
                            //   title: Text(
                            //       'Sing up and Sign in design in the flutter part 3'),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30.0),
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 10.0),
                      ExpansionTile(
                        title: Text(
                          "Instructor",
                          style: TextStyle(
                              fontSize: 18.0, fontWeight: FontWeight.bold),
                        ),
                        children: <Widget>[
                          ExpansionTile(
                            title: Text(
                              'Hussein abedHussein abed hilal, work in university on mustansiriayah inHussein abed Instructor Rating Review1 Student1 Cours',
                            ),
                            children: <Widget>[
                              ListTile(
                                title: Text(
                                  'Sing up and Sign in design in the flutter part 2',
                                ),
                              )
                            ],
                            // ),
                            // ListTile(
                            //   title: Text(
                            //       'Sing up and Sign in design in the flutter part 3'),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                // ExpansionTile(
                //   leading: Icon(Icons.cast_for_education),
                //   title: Text(" Introduction"),
                //   subtitle: Text(" "),
                //   children: <Widget>[
                //     Padding(
                //       padding: const EdgeInsets.all(8.0),
                //       child: Text(
                //         "*In Children use can use any flutter Widget",
                //         style: TextStyle(fontSize: 15),
                //       ),

                //     ),

                //     Padding(
                //       padding: const EdgeInsets.all(8.0),
                //       child: Text(
                //         "*In Children use can use any flutter Widget",
                //         style: TextStyle(fontSize: 15),
                //       ),
                //     ),
                //     // SizedBox(
                //     //   height: 10,
                //     // ),
                //     Center(
                //       child: Padding(
                //         padding: const EdgeInsets.all(8.0),
                //         child: Text(
                //             " * Children Widgets are expanded any flutter   ...."),
                //       ),
                //     )
                //   ],
                // ),
                // ExpansionTile(
                //   leading: Icon(Icons.integration_instructions_outlined),
                //   title: Text("Instructor Name"),
                //   subtitle: Text("  Course "),
                //   children: <Widget>[
                //     Padding(
                //       padding: const EdgeInsets.all(8.0),
                //       child: Text(
                //         "Abhiram Pillai",
                //         style: TextStyle(fontSize: 15),
                //       ),
                //     ),
                //     SizedBox(
                //       height: 15,
                //     ),
                //     Center(
                //       child: Padding(
                //         padding: const EdgeInsets.all(8.0),
                //         child: Text("Digital Marketing"),
                //       ),
                //     )
                //   ],
                // ),
                // Padding(
                //   padding: const EdgeInsets.all(8.0),
                //   child: ExpansionTile(
                //     leading: Icon(Icons.integration_instructions_sharp),
                //     title: Text("What you'll learn"),
                //     subtitle: Text("  Notes"),
                //     children: <Widget>[
                //       Padding(
                //         padding: const EdgeInsets.all(5.0),
                //         child: Text(
                //           " Google flutter , build a compete app for android and ios using the flutter with dart language Design and build app using flutter , dart language with firebaseGoogle flutterDart language and Google flutter ",
                //           style: TextStyle(fontSize: 15),
                //         ),
                //       ),
                //       // SizedBox(
                //       //   height: 20,
                //       // ),
                //       // Center(
                //       //   child: Text(
                //       //       "Children Widgets are expanded/ visible when hfhfhfExpansion Tile Widget is Clicked"),
                //       // )
                //     ],
                //   ),
                // ),
                // Row(
                //   mainAxisSize: MainAxisSize.max,
                //   children: <Widget>[
                //     Expanded(
                //       child: MaterialButton(
                //         color: Color(0xff33c0ba),
                //         elevation: 1,
                //         onPressed: () {},
                //         child: Container(
                //           padding: EdgeInsets.all(15.0),
                //           child: Text(
                //             "Buy",
                //             textAlign: TextAlign.center,
                //             style: TextStyle(
                //                 fontSize: 20.0,
                //                 color: Colors.white,
                //                 fontWeight: FontWeight.w500),
                //           ),
                //         ),
                //       ),
                //     ),
                //     Expanded(
                //       child: MaterialButton(
                //         color: Color(0xff33c0ba),
                //         elevation: 1,
                //         onPressed: () {},
                //         child: Container(
                //           padding: EdgeInsets.all(15.0),
                //           child: Text(
                //             "Add to cart",
                //             textAlign: TextAlign.center,
                //             style: TextStyle(
                //                 fontSize: 20.0,
                //                 color: Colors.white,
                //                 fontWeight: FontWeight.w500),
                //           ),
                //         ),
                //       ),
                //     ),
                //  ],
                // ),
              ],
            ),
          ),
          SizedBox(
            height: 30.0,
          ),
          Padding(
            padding: const EdgeInsets.all(0.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Text(
                    "Webinar ",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                        letterSpacing: 0,
                        fontWeight: FontWeight.w700),
                  ),
                ),
                // GestureDetector(
                //   onTap: () => Navigator.push(context,
                //       MaterialPageRoute(builder: (context) => Webinar())),
                //   child: Text(
                //     "See all",
                //     style: TextStyle(
                //         color: Colors.grey[500],
                //         fontSize: 14,
                //         letterSpacing: 0,
                //         fontWeight: FontWeight.bold),
                //   ),
                // )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage(
                            "assets/10.jpg",
                          ),
                          fit: BoxFit.cover),
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                            color: Colors.grey,
                            blurRadius: 5.0,
                            offset: Offset(0.75, 0.95))
                      ],
                      color: Colors.grey),
                  width: 150.0,
                  height: 100.0,
                ),
                Column(
                  children: [
                    Text(
                      "     Digital Maketing -part1 search",
                      style: TextStyle(fontSize: 15.0),
                    ),
                    Text(
                      "Freshers and job seekers",
                      style: TextStyle(fontSize: 15.0),
                    ),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(
            height: 20.0,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage(
                            "assets/11.jpg",
                          ),
                          fit: BoxFit.cover),
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                            color: Colors.grey,
                            blurRadius: 5.0,
                            offset: Offset(0.75, 0.95)),
                      ],
                      color: Colors.grey),
                  width: 150.0,
                  height: 100.0,
                ),
                Column(
                  children: [
                    Text(
                      "     Digital Maketing -part1 search",
                      style: TextStyle(fontSize: 15.0),
                    ),
                    Text(
                      "Freshers and job seekers",
                      style: TextStyle(fontSize: 15.0),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
