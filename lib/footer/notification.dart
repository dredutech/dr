import 'package:flutter/material.dart';

class NotificationPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.black,
            size: 35.0,
          ),
          onPressed: () => Navigator.pop(context),
        ),
        backgroundColor: Color(0xff33c0ba),
        title: Text(
          "Notifications",
          style: TextStyle(fontSize: 20.0, color: Colors.black),
        ),
      ),
      body: ListView(children: [
        SizedBox(
          height: 10.0,
        ),
        SizedBox(
          height: 20.0,
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            "Today",
            style: TextStyle(fontSize: 18.0),
          ),
        ),
        // SizedBox(
        //   height: 30.0,
        // ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage(
                          "assets/4.jpg",
                        ),
                        fit: BoxFit.cover),
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                          color: Colors.grey,
                          blurRadius: 5.0,
                          offset: Offset(0.40, 0.95))
                    ],
                    color: Colors.grey),
                width: 40.0,
                height: 40.0,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: [
                    Text(
                      "     Digital Maketing  Advertising. dfsgfdfgbd",
                      style: TextStyle(fontSize: 14.0),
                    ),
                    // Text(
                    //   "     SEO",
                    //   style: TextStyle(fontSize: 15.0),
                    // ),
                  ],
                ),
              ),
            ],
          ),
        ),
        // SizedBox(
        //   height: 10.0,
        // ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage(
                          "assets/4.jpg",
                        ),
                        fit: BoxFit.cover),
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                          color: Colors.grey,
                          blurRadius: 5.0,
                          offset: Offset(0.75, 0.95))
                    ],
                    color: Colors.grey),
                width: 40.0,
                height: 40.0,
              ),
              Column(
                children: [
                  Text(
                    "    Digital Maketing Content Strategy dasdsdada",
                    style: TextStyle(fontSize: 14.0),
                  ),
                  // Text(
                  //   "    SEO",
                  //   style: TextStyle(fontSize: 15.0),
                  // ),
                ],
              ),
            ],
          ),
        ),
        // SizedBox(
        //   height: 10.0,
        // ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage(
                          "assets/4.jpg",
                        ),
                        fit: BoxFit.cover),
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                          color: Colors.grey,
                          blurRadius: 5.0,
                          offset: Offset(0.75, 0.95))
                    ],
                    color: Colors.grey),
                width: 40.0,
                height: 40.0,
              ),
              Column(
                children: [
                  Text(
                    "     Digital Maketing,social Media assddfghcdfdd ",
                    style: TextStyle(fontSize: 14.0),
                  ),
                  // Text(
                  //   "      SEO",
                  //   style: TextStyle(fontSize: 15.0),
                  // ),
                ],
              ),
            ],
          ),
        ),
        // SizedBox(
        //   height: 30.0,
        // ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                "Yesterday",
                style: TextStyle(fontSize: 18.0),
              ),
            ),
            // SizedBox(
            //   height: 30.0,
            // ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(
                              "assets/4.jpg",
                            ),
                            fit: BoxFit.cover),
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                              color: Colors.grey,
                              blurRadius: 5.0,
                              offset: Offset(0.40, 0.95))
                        ],
                        color: Colors.grey),
                    width: 40.0,
                    height: 40.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: [
                        Text(
                          "     Digital Maketing -part1 sdfdsfsfsfccxczcx",
                          style: TextStyle(fontSize: 14.0),
                        ),
                        // Text(
                        //   "SEO",
                        //   style: TextStyle(fontSize: 15.0),
                        // ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            // SizedBox(
            //   height: 10.0,
            // ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(
                              "assets/4.jpg",
                            ),
                            fit: BoxFit.cover),
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                              color: Colors.grey,
                              blurRadius: 5.0,
                              offset: Offset(0.75, 0.95))
                        ],
                        color: Colors.grey),
                    width: 40.0,
                    height: 40.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: [
                        Text(
                          "     Digital Maketing -part1 setftgergergrd",
                          style: TextStyle(fontSize: 14.0),
                        ),
                        // Text(
                        //   "SEO",
                        //   style: TextStyle(fontSize: 15.0),
                        // ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            // SizedBox(
            //   height: 10.0,
            // ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(
                              "assets/4.jpg",
                            ),
                            fit: BoxFit.cover),
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                              color: Colors.grey,
                              blurRadius: 5.0,
                              offset: Offset(0.75, 0.95))
                        ],
                        color: Colors.grey),
                    width: 40.0,
                    height: 40.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: [
                        Text(
                          "      Digital Maketing  fsrtgrydrytyhtyhhf-part1",
                          style: TextStyle(fontSize: 14.0),
                        ),
                        // Text(
                        //   "SEO",
                        //   style: TextStyle(fontSize: 15.0),
                        // ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
        // SizedBox(
        //   height: 30.0,
        // ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "All",
                style: TextStyle(fontSize: 18.0),
              ),
              // SizedBox(
              //   height: 10.0,
              // ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(
                                "assets/4.jpg",
                              ),
                              fit: BoxFit.cover),
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                                color: Colors.grey,
                                blurRadius: 5.0,
                                offset: Offset(0.40, 0.95))
                          ],
                          color: Colors.grey),
                      width: 40.0,
                      height: 40.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: [
                          Text(
                            "     Digital Maketing -part1",
                            style: TextStyle(fontSize: 14.0),
                          ),
                          // Text(
                          //   "SEO",
                          //   style: TextStyle(fontSize: 15.0),
                          // ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              // SizedBox(
              //   height: 10.0,
              // ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(
                                "assets/4.jpg",
                              ),
                              fit: BoxFit.cover),
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                                color: Colors.grey,
                                blurRadius: 5.0,
                                offset: Offset(0.75, 0.95))
                          ],
                          color: Colors.grey),
                      width: 40.0,
                      height: 40.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: [
                          Text(
                            "     Digital Maketing -part1",
                            style: TextStyle(fontSize: 14.0),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              // SizedBox(
              //   height: 10.0,
              // ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(
                                "assets/4.jpg",
                              ),
                              fit: BoxFit.cover),
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                                color: Colors.grey,
                                blurRadius: 5.0,
                                offset: Offset(0.75, 0.95))
                          ],
                          color: Colors.grey),
                      width: 40.0,
                      height: 40.0,
                    ),
                    Column(
                      children: [
                        Text(
                          "     Digital Maketing -part1 dfrtrrdfttftrg",
                          style: TextStyle(fontSize: 14.0),
                        ),
                        // Text(
                        //   "SEO",
                        //   style: TextStyle(fontSize: 15.0),
                        // ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            // Text(
            //   title[rng.nextInt(3)],
            //   style: TextStyle(
            //     color: Colors.black,
            //     fontWeight: FontWeight.w700,
            //     letterSpacing: 1,
            //     fontSize: 18,
            //   ),
            //   maxLines: 1,
            //   overflow: TextOverflow.ellipsis,
            // ),
            // SizedBox(
            //   height: 14,
            // ),
          ],
        )
      ]),
    );
  }
}
// @author: Nishchal Siddharth Pandey
// Oct 12, 2020
// This will be used for displaying notification section

// class Notifications extends StatefulWidget {
//   @override
//   _NotificationsState createState() => _NotificationsState();
// }

// class _NotificationsState extends State<Notifications>
//     with AutomaticKeepAliveClientMixin {
//   String username;

//   @override
//   // void initState() {
//   //   super.initState();
//   //   instance = FirebaseFirestore.instance;
//   //   username = FirebaseAuth.instance.currentUser.displayName;
//   // }

//   @override
//   Widget build(BuildContext context) {
//     super.build(context);
//     return Scaffold(
//       body: RefreshIndicator(
//           onRefresh: () async {
//             setState(() {});
//           },
//           child: SingleChildScrollView(
//               physics: BouncingScrollPhysics(),
//               child: Column(children: [
//                 AppBar(
//                   title: Text('Notifications',
//                       style: TextStyle(fontWeight: FontWeight.bold)),
//                 ),
//                 StreamBuilder<QuerySnapshot>(
//                     stream: instance
//                         .collection('users/$username/notifications')
//                         .orderBy('time', descending: true)
//                         .snapshots(),
//                     builder: (context, snapshot) {
//                       return snapshot.connectionState == ConnectionState.waiting
//                           ? Center(
//                               child: CircularProgressIndicator(),
//                             )
//                           : ListView.builder(
//                               physics: NeverScrollableScrollPhysics(),
//                               shrinkWrap: true,
//                               itemCount: snapshot.data.docs.length,
//                               itemBuilder: (context, index) {
//                                 DocumentSnapshot documentSnapshot =
//                                     snapshot.data.docs[index];
//                                 String id = documentSnapshot.id;
//                                 int postRelated =
//                                     documentSnapshot.data()['postRelated'];
//                                 return NotificationCard(id, postRelated);
//                               });
//                     })
//               ]))),
//     );
//   }

//   @override
//   bool get wantKeepAlive => true;
// }
