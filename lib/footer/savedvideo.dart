import 'package:flutter/material.dart';

class SavedVideo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.black,
            size: 35.0,
          ),
          onPressed: () => Navigator.pop(context),
        ),
        backgroundColor: Color(0xff33c0ba),
        title: Text(
          "SavedVideos",
          style: TextStyle(fontSize: 20.0, color: Colors.black),
        ),
      ),
      body: ListView(
        children: [
          SizedBox(
            height: 10.0,
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage(
                            "assets/4.jpg",
                          ),
                          fit: BoxFit.cover),
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                            // color: Color(0xff33c0ba),
                            //blurRadius: 60.0,
                            offset: Offset(0.75, 0.95))
                      ],
                      color: Colors.grey),
                  width: 150.0,
                  height: 100,
                ),
                Column(
                  children: [
                    Text(
                      "Digital Maketing -part1",
                      style: TextStyle(fontSize: 20.0),
                    ),
                    Text(
                      "SEO",
                      style: TextStyle(fontSize: 18.0),
                    ),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(
            height: 30.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage(
                          "assets/4.jpg",
                        ),
                        fit: BoxFit.cover),
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                          // color: Color(0xff33c0ba),
                          //  blurRadius: 60.0,
                          offset: Offset(0.75, 0.95))
                    ],
                    color: Colors.grey),
                width: 150.0,
                height: 100,
              ),
              Column(
                children: [
                  Text(
                    "Digital Maketing -part1",
                    style: TextStyle(fontSize: 20.0),
                  ),
                  Text(
                    "SEO",
                    style: TextStyle(fontSize: 18.0),
                  ),
                ],
              ),
            ],
          ),
          SizedBox(
            height: 30.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage(
                          "assets/4.jpg",
                        ),
                        fit: BoxFit.cover),
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                          // color: Color(0xff33c0ba),
                          // blurRadius: 60.0,
                          offset: Offset(0.75, 0.95))
                    ],
                    color: Colors.grey),
                width: 150.0,
                height: 100,
              ),
              Column(
                children: [
                  Text(
                    "Digital Maketing -part1",
                    style: TextStyle(fontSize: 20.0),
                  ),
                  Text(
                    "SEO",
                    style: TextStyle(fontSize: 18.0),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
    // Column(
    //   mainAxisAlignment: MainAxisAlignment.start,
    //   crossAxisAlignment: CrossAxisAlignment.start,
    //   children: <Widget>[
    //     // Text(
    //     //   title[rng.nextInt(3)],
    //     //   style: TextStyle(
    //     //     color: Colors.black,
    //     //     fontWeight: FontWeight.w700,
    //     //     letterSpacing: 1,
    //     //     fontSize: 18,
    //     //   ),
    //     //   maxLines: 1,
    //     //   overflow: TextOverflow.ellipsis,
    //     // ),
    //     SizedBox(
    //       height: 14,
    //     ),

    //   ],
    // ),
  }
}

mixin SavedVideo1 {}
