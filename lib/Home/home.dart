import 'package:edu_app/Home/Griditem.dart';

///import 'package:edu_app/Screens/Welcome/welcome_screen.dart';
import 'package:edu_app/viewCourses/courseHomescreen.dart';
import 'package:edu_app/viewCourses/popular.dart';
import 'package:edu_app/viewCourses/trending.dart';
//import 'package:edu_app/Screens/Welcome/welcome_screen.dart';
//import 'package:edu_app/recently.dart';
//import 'package:edu_app/recently.dart';
import 'package:flutter/material.dart';

class CustomAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // ignore: unused_local_variable
    Size size = MediaQuery.of(context).size;
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: ClipRRect(
        child: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment(-1.0, -1.0),
                end: Alignment(1.0, -0.85),
                colors: [const Color(0xff145d7c), const Color(0xff33c0ba)],
                stops: [0.0, 1.0],
              ),
            ),
            child: ListView(
              children: [
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          // IconButton(icon: Icon(Icons.menu), onPressed: () {}),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(
                                width: 10.0,
                              ),
                            ],
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 50.0,
                      ),
                      Text(
                        'Digital University ',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 32,
                            letterSpacing: 0,
                            fontWeight: FontWeight.w700),
                      ),
                      SizedBox(
                        height: 6,
                      ),
                      Text(
                        'Continue To Digirich Education ',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Color(0xffE1F5FF),
                            fontSize: 18,
                            letterSpacing: 1.9,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
              ],
            )),
      ),
    );
  }
}

class Resources extends StatelessWidget {
  final _controller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    // ignore: unused_local_variable
    Size size = MediaQuery.of(context).size;
    return Scaffold(
        body: Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        // Box decoration takes a gradient
        gradient: LinearGradient(
          // Where the linear gradient begins and ends
          begin: Alignment.topLeft,
          end: Alignment.topRight,
          // Add one stop for each color. Stops should increase from 0 to 1
          // stops: [0.4, 0.7, 0.5, 0.5],
          colors: [
            // Colors are easy thanks to Flutter's Colors class.
            Color(0xfffafdff),
            Color(0xfffafdff),
            // Color(0xffE7FFFF),
            // Color(0xffE7FFFF),
          ],
        ),
      ),
      child: SingleChildScrollView(
        physics: ClampingScrollPhysics(),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(boxShadow: [
                BoxShadow(
                  color: Colors.grey,
                  blurRadius: 20.0,
                ),
              ]),
              margin: EdgeInsets.only(top: 16.0, left: 16.0, right: 16.0),
              height: 62,
              child: new TextField(
                controller: _controller,
                decoration: InputDecoration(
                  suffixIcon: Padding(
                    padding: const EdgeInsets.only(
                        right: 4.0, top: 2, bottom: 2, left: 2.0),
                    child: SizedBox(
                      width: 64.0,
                      // ignore: deprecated_member_use
                      child: FlatButton(
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(12))),
                          color: Color(0xffeef1f3),
                          onPressed: () {},
                          child: Center(
                            child: Icon(
                              Icons.search,
                              size: 32,
                              color: Colors.black,
                            ),
                          )),
                    ),
                  ),
                  filled: true,
                  fillColor: Colors.white,
                  hintText: 'Search ',
                  hintStyle: TextStyle(
                      fontSize: 18,
                      color: Colors.black45,
                      fontWeight: FontWeight.w600),
                  contentPadding: EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
                  border: OutlineInputBorder(
                    borderSide: BorderSide.none,
                    borderRadius: BorderRadius.circular(12.0),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            Padding(
              padding: const EdgeInsets.all(0.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                      "Courses ",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                          letterSpacing: 0,
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                  GestureDetector(
                    onTap: () => Navigator.push(context,
                        MaterialPageRoute(builder: (context) => HomeScreen1())),
                    child: Text(
                      "See all",
                      style: TextStyle(
                          color: Colors.grey[500],
                          fontSize: 14,
                          letterSpacing: 0,
                          fontWeight: FontWeight.bold),
                    ),
                  )
                ],
              ),
            ),

            Row(
              children: [
                Column(
                  children: [
                    GridItem("Digital Marketing ", "200", "assets/web.jpg"),
                    GridItem("Digital Marketing ", "200", "assets/5.jpg"),
                    GridItem("Digital Marketing ", "200", "assets/4.jpg"),
                  ],
                ),
                Column(
                  children: [
                    GridItem("Digital Marketing ", "200", "assets/5.jpg"),
                    GridItem("Digital Marketing ", "200", "assets/6.jpg"),
                    GridItem("Digital Marketing ", "200", "assets/7.jpg"),
                  ],
                ),
                Column(
                  children: [
                    GridItem("Digital Marketing ", "200", "assets/8.jpg"),
                    GridItem("Digital Marketing ", "200", "assets/9.jpg"),
                    GridItem("Digital Marketing ", "200", "assets/10.jpg"),
                  ],
                )
              ],
            ),

            Padding(
              padding: const EdgeInsets.all(0.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                      "Popular videos",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 20,
                          letterSpacing: 0,
                          fontWeight: FontWeight.w700),
                    ),
                  ),
                  GestureDetector(
                    onTap: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PopularVideos())),
                    child: Text(
                      "See all",
                      style: TextStyle(
                          color: Colors.grey[500],
                          fontSize: 14,
                          letterSpacing: 0,
                          fontWeight: FontWeight.bold),
                    ),
                  )
                ],
              ),
            ),

            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                height: 210,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: 3,
                  shrinkWrap: true,
                  itemBuilder: (BuildContext context, int index) {
                    return CourseCard(
                        "DM-Search Engine Basics ", "", "assets/11.jpg");
                  },
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "Trending videos",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                        letterSpacing: 0,
                        fontWeight: FontWeight.w700),
                  ),
                  // GestureDetector(
                  //   onTap: () {
                  //     Navigator.push(
                  //       context,
                  //       MaterialPageRoute(
                  //         builder: (context) {
                  //           return WelcomeScreen();
                  //         },
                  //       ),
                  //     );
                  //   },
                  // ),
                  GestureDetector(
                    onTap: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => TrendingVideos())),
                    child: Text(
                      "See all",
                      style: TextStyle(
                          color: Colors.grey[500],
                          fontSize: 14,
                          letterSpacing: 0,
                          fontWeight: FontWeight.bold),
                    ),
                  )
                ],
              ),
            ),
            Container(
              height: 210,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: 3,
                shrinkWrap: true,
                itemBuilder: (BuildContext context, int index) {
                  return CourseCard(
                      "HTML+Static Website Creation ", "10", "assets/10.jpg");
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "All videos",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                        letterSpacing: 0,
                        fontWeight: FontWeight.w700),
                  ),
                  // GestureDetector(
                  //   onTap: () {
                  //     Navigator.push(
                  //       context,
                  //       MaterialPageRoute(
                  //         builder: (context) {
                  //           return WelcomeScreen();
                  //         },
                  //       ),
                  //     );
                  //   },
                  // ),
                  GestureDetector(
                    onTap: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => TrendingVideos())),
                    child: Text(
                      "See all",
                      style: TextStyle(
                          color: Colors.grey[500],
                          fontSize: 14,
                          letterSpacing: 0,
                          fontWeight: FontWeight.bold),
                    ),
                  )
                ],
              ),
            ),

            SizedBox(
              height: 10.0,
            ),
            // Padding(
            //   padding: const EdgeInsets.all(8.0),
            //   child: Text(
            //     "Webinar",
            //     style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
            //   ),
            // ),
            SizedBox(
              height: 20.0,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(
                              "assets/10.jpg",
                            ),
                            fit: BoxFit.cover),
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                              color: Colors.grey,
                              blurRadius: 5.0,
                              offset: Offset(0.75, 0.95))
                        ],
                        color: Colors.grey),
                    width: 150.0,
                    height: 100.0,
                  ),
                  Column(
                    children: [
                      Text(
                        "     Digital Maketing -part1 search",
                        style: TextStyle(fontSize: 15.0),
                      ),
                      Text(
                        "Freshers and job seekers",
                        style: TextStyle(fontSize: 15.0),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(
                              "assets/11.jpg",
                            ),
                            fit: BoxFit.cover),
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                              color: Colors.grey,
                              blurRadius: 5.0,
                              offset: Offset(0.75, 0.95))
                        ],
                        color: Colors.grey),
                    width: 150.0,
                    height: 100.0,
                  ),
                  Column(
                    children: [
                      Text(
                        "     Digital Maketing -part1 search",
                        style: TextStyle(fontSize: 15.0),
                      ),
                      Text(
                        "Freshers and job seekers",
                        style: TextStyle(fontSize: 15.0),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            // ListView.builder(
            //   //scrollDirection: Axis.vertical,
            //   itemCount: 3,
            //   shrinkWrap: true,
            //   padding: EdgeInsets.all(1.0),
            //   //  physics: NeverScrollableScrollPhysics(),
            //   itemBuilder: (BuildContext context, int index) {
            //     return Recently();
            //   },
            // ),
          ],
        ),
      ),
    ));
  }
}

class Griditem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            body: GridView.count(
                crossAxisCount: 3,
                crossAxisSpacing: 4.0,
                mainAxisSpacing: 8.0,
                children: List.generate(choices.length, (index) {
                  return Center(
                    child: SelectCard(choice: choices[index]),
                  );
                }))));
  }
}

class Choice {
  const Choice({this.title, this.icon});
  final String title;
  final IconData icon;
}

const List<Choice> choices = const <Choice>[
  const Choice(title: 'Home', icon: Icons.home),
  const Choice(title: 'Contact', icon: Icons.contacts),
  const Choice(title: 'Map', icon: Icons.map),
  const Choice(title: 'Phone', icon: Icons.phone),
  const Choice(title: 'Camera', icon: Icons.camera_alt),
  const Choice(title: 'Setting', icon: Icons.settings),
  const Choice(title: 'Album', icon: Icons.photo_album),
  const Choice(title: 'WiFi', icon: Icons.wifi),
];

class SelectCard extends StatelessWidget {
  const SelectCard({Key key, this.choice}) : super(key: key);
  final Choice choice;

  @override
  Widget build(BuildContext context) {
    // ignore: deprecated_member_use
    final TextStyle textStyle = Theme.of(context).textTheme.display1;
    return Card(
        color: Colors.orange,
        child: Center(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                    child:
                        Icon(choice.icon, size: 50.0, color: textStyle.color)),
                Text(choice.title, style: textStyle),
              ]),
        ));
  }
}

class CourseCard extends StatelessWidget {
  final String title, count, imagePath;

  CourseCard(
    this.title,
    this.count,
    this.imagePath,
  );

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            height: 140.0,
            width: 250.0,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage(imagePath), fit: BoxFit.cover),
                borderRadius: BorderRadius.circular(24),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: Colors.grey,
                      blurRadius: 15.0,
                      offset: Offset(0.75, 0.95))
                ],
                color: Colors.grey),
          ),
          SizedBox(
            height: 16,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 4.0),
            child: Text(
              '$title',
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  letterSpacing: 1.9,
                  fontSize: 16.0),
            ),
          ),
        ],
      ),
    );
  }
}
