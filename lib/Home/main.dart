import 'package:edu_app/Home/AddToCart.dart';
import 'package:edu_app/Home/drawer_item.dart';
import 'package:edu_app/Home/home.dart';
import 'package:edu_app/Logo/logo.dart';
import 'package:edu_app/MenuBar/About.dart';
import 'package:edu_app/MenuBar/certificates.dart';
import 'package:edu_app/MenuBar/profile.dart';
import 'package:edu_app/MenuBar/purchased.dart';
import 'package:edu_app/MenuBar/settings.dart';
import 'package:edu_app/footer/notification.dart';
import 'package:edu_app/footer/ongoing.dart';
import 'package:edu_app/footer/savedvideo.dart';
import 'package:edu_app/login/welcome_screen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: Logo(),
  ));
}

class MyApp2 extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
          primarySwatch: Colors.blue,
          // ignore: deprecated_member_use
          textTheme: TextTheme(title: TextStyle(fontFamily: "Nunito"))),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // ignore: unused_local_variable
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(250.0),
        child: AppBar(
          title: Text(
            'Dr Edutech ',
            textAlign: TextAlign.center,
            style: TextStyle(
                fontWeight: FontWeight.bold,
                height: 2,
                fontSize: 30,
                color: Colors.white),
          ),
          centerTitle: true,
          flexibleSpace: CustomAppBar(),
          actions: [
            IconButton(
              onPressed: () => Navigator.push(context,
                  MaterialPageRoute(builder: (context) => CartOnePage())),
              icon: Icon(
                Icons.add_shopping_cart,
                color: Colors.white,
              ),
            ),
          ],
        ),
      ),
      drawer: Drawer(
        child: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                const Color(0xff33c0ba),
                const Color(0xff145d7c),
              ],
              stops: [0.0, 1.0],
            ),
          ),
          child: ListView(children: <Widget>[
            DrawerHeader(
              child: Text(''),
              decoration: BoxDecoration(
                color: Color(0xff33c0ba),
              ),
            ),
            // SizedBox(
            //   height: 100,
            // ),
            // ignore: missing_required_param
            DrawerItem(
              text: 'My Profile',
              press: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ProfileEightPage()));
              },
            ),
            // ignore: missing_required_param
            DrawerItem(
              text: 'Certificates',
              press: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Certificates()));
              },
            ),
            // ignore: missing_required_param
            DrawerItem(
              text: 'Purchased',
              press: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => PurchasedVideo()));
              },
            ),
            // ignore: missing_required_param
            DrawerItem(
              text: 'Settings',
              press: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => SettingsTwoPage()));
              },
            ),
            // ignore: missing_required_param
            DrawerItem(
              text: 'Logout',
              press: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => WelcomeScreen()));
              },
            ),
            // ignore: missing_required_param
            DrawerItem(
              text: 'Aboutus',
              press: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => About()));
              },
            ),
          ]),
        ),
      ),
      body: Stack(
        children: [
          Resources(),
        ],
      ),
      bottomNavigationBar: Container(
        padding: EdgeInsets.symmetric(vertical: 10),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
          // boxShadow: [
          //   BoxShadow(
          //     offset: Offset(0, -15),
          //     blurRadius: 20,
          //     color: Color(0xFFDADADA).withOpacity(0.15),
          //   ),
          // ],
        ),
        child: SafeArea(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(
                width: 0.01,
              ),
              IconButton(
                  icon: Icon(Icons.home),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => MyHomePage(),
                      ),
                    );
                  }),
              SizedBox(
                width: 0.01,
              ),
              IconButton(
                  icon: Icon(Icons.bookmark),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => SavedVideo(),
                      ),
                    );
                  }),
              SizedBox(
                width: 0.01,
              ),
              IconButton(
                  icon: Icon(Icons.notifications),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => NotificationPage(),
                      ),
                    );
                  }),
              SizedBox(
                width: 0.01,
              ),
              IconButton(
                  icon: Icon(Icons.play_arrow),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => OngoingPage(),
                      ),
                    );
                  }),
              SizedBox(
                width: 0.01,
              ),
            ],
          ),
        ),
      ),
    );
  }

  widget({CartOnePage child}) {}
}

class MyApp1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Expansion View',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
    );
  }
}
