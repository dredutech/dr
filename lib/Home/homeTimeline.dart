import 'package:flutter/material.dart';

class TimeLinePage extends StatefulWidget {
  @override
  _TimeLinePageState createState() => _TimeLinePageState();
}

class _TimeLinePageState extends State<TimeLinePage> {
  get circularProgress => null;

  @override
  Widget build(context) {
    var circularProgress2 = circularProgress;
    return Scaffold(
      appBar: AppBar(
        title: Text('Aju'),
      ),
      body: circularProgress2(),
    );
  }
}
