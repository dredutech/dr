//import 'package:edu_app/Main1.dart';
import 'package:edu_app/Exam/exam/examinstruction.dart';
import 'package:edu_app/login/rounded_button.dart';
import 'package:edu_app/main.dart';
//import 'package:edu_app/Resources.dart';
import 'package:flutter/material.dart';
//import 'background.dart';

class Body extends StatefulWidget {
  const Body({
    Key key,
  }) : super(key: key);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Colors.white,
              size: 35.0,
            ),
            onPressed: () => Navigator.pop(context),
          ),
          backgroundColor: Color(0xff33c0ba),
          title: Text(
            "Exam",
            style: TextStyle(fontSize: 24.0, color: Colors.white),
          ),
        ),
        body: ListView(children: [
          SizedBox(
            height: 0.0,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 30.0,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage(
                                  "assets/exam.jpeg",
                                ),
                                fit: BoxFit.cover),
                            borderRadius: BorderRadius.circular(5),
                            boxShadow: <BoxShadow>[
                              BoxShadow(
                                  // color: Color(0xff33c0ba),
                                  // blurRadius: 65.0,
                                  offset: Offset(0.90, 0.90))
                            ],
                            color: Colors.black),
                        width: 370.0,
                        height: 270.0,
                      ),
                      SizedBox(
                        height: 30.0,
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 30.0,
                ),

                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "Congratulations",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      // fontFamily: 'Open Sans',
                      fontSize: 30,
                      color: const Color(0xff000000),
                      //fontWeight: FontWeight.w700,
                    ),
                    textAlign: TextAlign.right,
                  ),
                ),

                Text(
                  "for complete your course",
                  style: TextStyle(
                      color: Color(0xff000000), fontWeight: FontWeight.bold),
                ),
                RoundedButton(
                  text: "Start Now",
                  press: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) {
                          return Examinstruction();
                        },
                      ),
                    );
                  },
                ),

                // SizedBox(height: size.height * 0.02),
                // SizedBox(height: size.height * 0.02),
                RoundedButton(
                  text: "Starts Later",
                  press: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) {
                          return MyHomePage();
                        },
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
        ]));
  }
}

// class Body extends StatefulWidget {
//   const Body({
//     Key key,
//   }) : super(key: key);

//   @override
//   _BodyState createState() => _BodyState();
// }

// class _BodyState extends State<Body> {
//   @override
//   Widget build(BuildContext context) {
//     Size size = MediaQuery.of(context).size;
//     return Background(
//       appBar: AppBar(
//         leading: IconButton(
//           icon: Icon(
//             Icons.arrow_back,
//             color: Colors.white,
//             size: 35.0,
//           ),
//           onPressed: () => Navigator.pop(context),
//         ),
//         backgroundColor: Colors.white,
//       ),
//       child: SingleChildScrollView(
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: <Widget>[
//             SizedBox(height: size.height * 0.30),
//             Text(
//               "Congratulation",
//               style: TextStyle(
//                 fontWeight: FontWeight.bold,
//                 // fontFamily: 'Open Sans',
//                 fontSize: 30,
//                 color: const Color(0xff000000),
//                 //fontWeight: FontWeight.w700,
//               ),
//               textAlign: TextAlign.left,
//             ),
//             Text(
//               "for complete your course",
//               style: TextStyle(
//                   color: Color(0xff000000), fontWeight: FontWeight.bold),
//             ),
//             SizedBox(height: size.height * 0.03),
//             RoundedButton(
//               text: "Start Now",
//               press: () {
//                 Navigator.push(
//                   context,
//                   MaterialPageRoute(
//                     builder: (context) {
//                       return Examinstruction();
//                     },
//                   ),
//                 );
//               },
//             ),

//             // SizedBox(height: size.height * 0.02),
//             // SizedBox(height: size.height * 0.02),
//             RoundedButton(
//               text: "Starts Later",
//               press: () {
//                 Navigator.push(
//                   context,
//                   MaterialPageRoute(
//                     builder: (context) {
//                       return Homepage();
//                     },
//                   ),
//                 );
//               },
//             ),
//             // AlreadyHaveAnAccountCheck(
//             //   press: () {
//             //     Navigator.push(
//             //       context,
//             //       MaterialPageRoute(
//             //         builder: (context) {
//             //           return WelcomeScreen();
//             //         },
//             //       ),
//             //     );
//             //   },
//             // ),
//           ],
//         ),
//       ),
//     );
//   }
// }
