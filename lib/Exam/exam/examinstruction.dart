import 'package:edu_app/Exam/exam/exam_screen.dart';
import 'package:edu_app/login/rounded_button.dart';
import 'package:flutter/material.dart';
//import 'package:flutter_auth/Screens/Exam/components/exam.dart';

class Examinstruction extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Colors.white,
              size: 35.0,
            ),
            onPressed: () => Navigator.pop(context),
          ),
          backgroundColor: Color(0xff33c0ba),
          title: Text(
            "Exam Instructions",
            style: TextStyle(fontSize: 24.0, color: Colors.white),
          ),
        ),
        body: ListView(children: [
          SizedBox(
            height: 10.0,
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(
                              "assets/instruction.jpeg",
                            ),
                            fit: BoxFit.cover),
                        borderRadius: BorderRadius.circular(70),
                        color: Color(0xff33c0ba)),
                    width: 250.0,
                    height: 250,
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(" 20 Minutes",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  // fontFamily: 'Open Sans',
                  fontSize: 25,
                  color: const Color(0xff33c0ba),
                  //  style: titleTextStyle,
                ),
                textAlign: TextAlign.center),
          ),
          SizedBox(
            height: 30.0,
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(
              " 1. Attend all the questions",
              //  style: titleTextStyle,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(
              " 2. Read all the information carefully ",
              //  style: titleTextStyle,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(
              " 3. 20 minutes exam ",
              //  style: titleTextStyle,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(
              " 4. --",
              //  style: titleTextStyle,
            ),
          ),
          SizedBox(
            height: 10.0,
          ),
          RoundedButton(
            text: "Start",
            press: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) {
                    return ExamScreen();
                  },
                ),
              );
            },
          ),
        ]));
  }
}
