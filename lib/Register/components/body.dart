//import 'package:edu_app/components/signup.dart';
import 'package:edu_app/Register/components/background.dart';
import 'package:edu_app/login/already_have_an_account_acheck.dart';
import 'package:edu_app/login/otp.dart';
import 'package:edu_app/login/rounded_button.dart';
import 'package:edu_app/login/rounded_input_field.dart';
import 'package:edu_app/login/rounded_password_field.dart';
import 'package:edu_app/login/welcome_screen.dart';
import 'package:flutter/material.dart';




class Body extends StatefulWidget {
  const Body({
    Key key,
  }) : super(key: key);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  //UserMode _user;
  final TextEditingController nameController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController mobileController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  // Future<UserMode> createUser(
  //     String email, String username, var mobile, var password) async {
  //   final String apiurl = "http://127.0.0.1:8000/api/accounts/register/";
  //   final response = await http.post(apiurl, body: {
  //     'email': email,
  //     'username': username,
  //     'mobile': mobile,
  //     'password': password
  //   });
  //   if (response.statusCode == 200) {
  //     final String responseString = response.body;
  //     return userModeFromJson(responseString);
  //   } else
  //     return null;
  // }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    var roundedButton = RoundedButton(
      text: "Register",
      style: TextStyle(
        fontFamily: 'Helvetica Neue',
        fontSize: 59,
        color: const Color(0xffffffff),
        fontWeight: FontWeight.w700,
      ),
      // press: () async {
      //   final String username = nameController.text;
      //   final String email = emailController.text;
      //   final String mobile = mobileController.text;
      //   final String password = passwordController.text;
      //   final UserMode user =
      //       await createUser(email, username, mobile, password);
      //   setState(() {
      //     _user = user;
      //   });
      //   print("ggjhkjlkjl");
      //   Navigator.push(
      //     context,
      //     MaterialPageRoute(
      //       builder: (context) => WelcomeScreen(),
      //     ),
      //   );
      // },
      press: () {
        print("sssss");
      },
    );
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(height: size.height * 0.03),
            SizedBox(height: size.height * 0.03),
            RoundedInputField(
              hintText: "Name",
              controll: nameController,
              onChanged: (value) {},
            ),
            RoundedInputField(
              hintText: "Email",
              controll: emailController,
              onChanged: (value) {},
            ),
            RoundedPasswordField(
              controll: passwordController,
              onChanged: (value) {},
            ),
            RoundedInputField(
              hintText: "Mobile",
              controll: mobileController,
              //fontSize: 41,
              onChanged: (value) {},
            ),
            roundedButton,
            SizedBox(height: size.height * 0.03),
            AlreadyHaveAnAccountCheck(
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return WelcomeScreen();
                    },
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}

class OTPFields extends StatefulWidget {
  const OTPFields({
    Key key,
  }) : super(key: key);

  @override
  _OTPFieldsState createState() => _OTPFieldsState();
}

class _OTPFieldsState extends State<OTPFields> {
  FocusNode pin2FN;
  FocusNode pin3FN;
  FocusNode pin4FN;
  final pinStyle = TextStyle(fontSize: 32, fontWeight: FontWeight.bold);

  @override
  void initState() {
    super.initState();
    pin2FN = FocusNode();
    pin3FN = FocusNode();
    pin4FN = FocusNode();
  }

  @override
  void dispose() {
    super.dispose();
    pin2FN?.dispose();
    pin3FN?.dispose();
    pin4FN?.dispose();
  }

  void nextField(String value, FocusNode focusNode) {
    if (value.length == 1) {
      focusNode.requestFocus();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      child: Column(
        children: [
          const SizedBox(height: 10.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              SizedBox(
                width: 40,
                child: TextFormField(
                  autofocus: true,
                  style: pinStyle,
                  keyboardType: TextInputType.number,
                  textAlign: TextAlign.center,
                  decoration: inputDecoration,
                  onChanged: (value) {
                    nextField(value, pin2FN);
                  },
                ),
              ),
              SizedBox(
                width: 40,
                child: TextFormField(
                  autofocus: true,
                  style: pinStyle,
                  keyboardType: TextInputType.number,
                  textAlign: TextAlign.center,
                  decoration: inputDecoration,
                  onChanged: (value) {
                    nextField(value, pin2FN);
                  },
                ),
              ),
              SizedBox(
                width: 40,
                child: TextFormField(
                  autofocus: true,
                  style: pinStyle,
                  keyboardType: TextInputType.number,
                  textAlign: TextAlign.center,
                  decoration: inputDecoration,
                  onChanged: (value) {
                    nextField(value, pin2FN);
                  },
                ),
              ),
              SizedBox(
                width: 40,
                child: TextFormField(
                  focusNode: pin2FN,
                  style: pinStyle,
                  keyboardType: TextInputType.number,
                  textAlign: TextAlign.center,
                  decoration: inputDecoration,
                  onChanged: (value) => nextField(value, pin3FN),
                ),
              ),
              SizedBox(
                width: 40,
                child: TextFormField(
                  focusNode: pin3FN,
                  style: pinStyle,
                  keyboardType: TextInputType.number,
                  textAlign: TextAlign.center,
                  decoration: inputDecoration,
                  onChanged: (value) => nextField(value, pin4FN),
                ),
              ),
              SizedBox(
                width: 40,
                child: TextFormField(
                  focusNode: pin4FN,
                  style: pinStyle,
                  keyboardType: TextInputType.number,
                  textAlign: TextAlign.center,
                  decoration: inputDecoration,
                  onChanged: (value) {
                    if (value.length == 1) {
                      pin4FN.unfocus();
                    }
                  },
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

// import 'package:flutter/material.dart';
// import 'package:flutter_auth/Screens/Welcome/welcome_screen.dart';
// //import 'package:flutter_auth/Screens/Register/register_screen.dart';
// // import 'package:flutter_auth/Screens/components/Register/background.dart';
// import 'package:flutter_auth/components/already_have_an_account_acheck.dart';
// // import 'package:flutter_auth/components/otp.dart';
// import 'package:flutter_auth/components/rounded_button.dart';
// import 'package:flutter_auth/components/rounded_input_field.dart';
// import 'package:flutter_auth/components/rounded_password_field.dart';

// import 'background.dart';
// //import '../register_screen.dart';

// class RegisterScreen extends StatefulWidget {
//   const RegisterScreen({
//     Key key,
//   }) : super(key: key);

//   @override
//   _BodyState createState() => _BodyState();
// }

// class _BodyState extends State<RegisterScreen> {
//   @override
//   Widget build(BuildContext context) {
//     Size size = MediaQuery.of(context).size;
//     var roundedButton = RoundedButton(
//       backgroundColor: const Color(0xff33c0ba),
//       text: "Register",
//       style: TextStyle(
//         fontFamily: 'Helvetica Neue',
//         fontSize: 59,
//         // colors: [const Color(0xff145d7c), const Color(0xff33c0ba)],
//         color: const Color(0xff145d7c),
//         fontWeight: FontWeight.w700,
//       ),
//       press: () {
//         Navigator.push(
//           context,
//           MaterialPageRoute(
//             builder: (context) => WelcomeScreen(),
//           ),
//         );
//       },
//     );
//     return Background(
//       child: SingleChildScrollView(
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: <Widget>[
//             SizedBox(height: size.height * 0.03),
//             SizedBox(height: size.height * 0.03),
//             RoundedInputField(
//               hintText: "Name",
//               onChanged: (value) {},
//             ),
//             RoundedInputField(
//               hintText: "Email",
//               onChanged: (value) {},
//             ),
//             RoundedPasswordField(
//               onChanged: (value) {},
//             ),
//             RoundedInputField(
//               hintText: "Mobile",
//               //fontSize: 41,
//               onChanged: (value) {},
//             ),
//             roundedButton,
//             SizedBox(height: size.height * 0.03),
//             AlreadyHaveAnAccountCheck(
//               press: () {
//                 Navigator.push(
//                   context,
//                   MaterialPageRoute(
//                     builder: (context) {
//                       return WelcomeScreen();
//                     },
//                   ),
//                 );
//               },
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }

// // class OTPFields extends StatefulWidget {
// //   const OTPFields({
// //     Key key,
// //   }) : super(key: key);

// //   @override
// //   _OTPFieldsState createState() => _OTPFieldsState();
// // }

// // class _OTPFieldsState extends State<OTPFields> {
// //   FocusNode pin2FN;
// //   FocusNode pin3FN;
// //   FocusNode pin4FN;
// //   final pinStyle = TextStyle(fontSize: 32, fontWeight: FontWeight.bold);

// //   @override
// //   void initState() {
// //     super.initState();
// //     pin2FN = FocusNode();
// //     pin3FN = FocusNode();
// //     pin4FN = FocusNode();
// //   }

// //   @override
// //   void dispose() {
// //     super.dispose();
// //     pin2FN?.dispose();
// //     pin3FN?.dispose();
// //     pin4FN?.dispose();
// //   }

// //   void nextField(String value, FocusNode focusNode) {
// //     if (value.length == 1) {
// //       focusNode.requestFocus();
// //     }
// //   }

// //   @override
// //   Widget build(BuildContext context) {
// //     return Form(
// //       child: Column(
// //         children: [
// //           const SizedBox(height: 10.0),
// //           Row(
// //             mainAxisAlignment: MainAxisAlignment.spaceAround,
// //             children: [
// //               SizedBox(
// //                 width: 40,
// //                 child: TextFormField(
// //                   autofocus: true,
// //                   style: pinStyle,
// //                   keyboardType: TextInputType.number,
// //                   textAlign: TextAlign.center,
// //                   decoration: inputDecoration,
// //                   onChanged: (value) {
// //                     nextField(value, pin2FN);
// //                   },
// //                 ),
// //               ),
// //               SizedBox(
// //                 width: 40,
// //                 child: TextFormField(
// //                   autofocus: true,
// //                   style: pinStyle,
// //                   keyboardType: TextInputType.number,
// //                   textAlign: TextAlign.center,
// //                   decoration: inputDecoration,
// //                   onChanged: (value) {
// //                     nextField(value, pin2FN);
// //                   },
// //                 ),
// //               ),
// //               SizedBox(
// //                 width: 40,
// //                 child: TextFormField(
// //                   autofocus: true,
// //                   style: pinStyle,
// //                   keyboardType: TextInputType.number,
// //                   textAlign: TextAlign.center,
// //                   decoration: inputDecoration,
// //                   onChanged: (value) {
// //                     nextField(value, pin2FN);
// //                   },
// //                 ),
// //               ),
// //               SizedBox(
// //                 width: 40,
// //                 child: TextFormField(
// //                   focusNode: pin2FN,
// //                   style: pinStyle,
// //                   keyboardType: TextInputType.number,
// //                   textAlign: TextAlign.center,
// //                   decoration: inputDecoration,
// //                   onChanged: (value) => nextField(value, pin3FN),
// //                 ),
// //               ),
// //               SizedBox(
// //                 width: 40,
// //                 child: TextFormField(
// //                   focusNode: pin3FN,
// //                   style: pinStyle,
// //                   keyboardType: TextInputType.number,
// //                   textAlign: TextAlign.center,
// //                   decoration: inputDecoration,
// //                   onChanged: (value) => nextField(value, pin4FN),
// //                 ),
// //               ),
// //               SizedBox(
// //                 width: 40,
// //                 child: TextFormField(
// //                   focusNode: pin4FN,
// //                   style: pinStyle,
// //                   keyboardType: TextInputType.number,
// //                   textAlign: TextAlign.center,
// //                   decoration: inputDecoration,
// //                   onChanged: (value) {
// //                     if (value.length == 1) {
// //                       pin4FN.unfocus();
// //                     }
// //                   },
// //                 ),
// //               ),
// //             ],
// //           ),
// //         ],
// //       ),
// //     );
// //   }
// // }
