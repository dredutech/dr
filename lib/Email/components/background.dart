import 'package:flutter/material.dart';
// import 'package:flutter_svg/svg.dart';

class Background extends StatelessWidget {
  final Widget child;
  const Background({
    Key key,
    @required this.child,
    AppBar appBar,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      height: size.height,
      width: double.infinity,
      // Here i can use size.width but use double.infinity because both work as a same
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          // Positioned(
          //   top: 0,
          //   left: 0,
          //   child: SvgPicture.asset(
          //     "assets/icons/arrow.svg",
          //   ),
          // ),
          // Positioned(
          //   bottom: 0,
          //   left: 0,
          //   child: Image.asset(
          //     "assets/images/main_bottom.png",
          //     width: size.width * 0.25,
          //   ),
          // ),
          child,
        ],
      ),
    );
  }
}
// import 'package:flutter/material.dart';
// // import 'package:flutter_svg/svg.dart';

// class Background extends StatelessWidget {
//   final Widget child;
//   const Background({
//     Key key,
//     @required this.child,
//     AppBar appBar,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     Size size = MediaQuery.of(context).size;
//     return Container(
//       height: size.height,
//       width: double.infinity,
//       // Here i can use size.width but use double.infinity because both work as a same
//       child: Stack(
//         alignment: Alignment.center,
//         children: <Widget>[
//           Container(
//             decoration: BoxDecoration(
//               gradient: LinearGradient(
//                 colors: [const Color(0xff145d7c), const Color(0xff33c0ba)],
//                 stops: [0.0, 1.0],
//               ),
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }
