import 'package:edu_app/Email/components/background.dart';
import 'package:edu_app/login/AlreadyKnowPasswordCheck.dart';
import 'package:edu_app/login/otp.dart';
import 'package:edu_app/login/rounded_button.dart';
import 'package:edu_app/login/rounded_input_field.dart';
import 'package:edu_app/login/welcome_screen.dart';
import 'package:flutter/material.dart';
//import 'package:flutter_auth/Screens/home/homepage.dart';
// import 'package:flutter_auth/Screens/Welcome/welcome_screen.dart';
// import 'package:flutter_svg/svg.dart';

class Body extends StatefulWidget {
  const Body({
    Key key,
  }) : super(key: key);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            // SizedBox(height: size.height * 0.03),
            SizedBox(height: size.height * 0.30),
            RoundedInputField(
              hintText: "Enter your email id",
              onChanged: (value) {},
            ),
            // SizedBox(height: size.height * 0.01),
            // SvgPicture.asset(
            //   "assets/icons/arrow.svg",
            //   height: size.height * 0.1,
            // ),
            Transform.translate(
              offset: Offset(517.0, 1125.0),
              child: Text(
                'Enter your email id',
                style: TextStyle(
                  fontFamily: 'Open Sans',
                  fontSize: 46,
                  color: const Color(0x57000000),
                ),
                textAlign: TextAlign.left,
              ),
            ),
            Transform.translate(
              offset: Offset(630.0, 940.0),
              child: Text(
                'Email',
                style: TextStyle(
                  fontFamily: 'Open Sans',
                  fontSize: 71,
                  // color: [const Color(0xff145d7c), const Color(0xff33c0ba)],
                  color: const Color(0xff145d7c),
                ),
                textAlign: TextAlign.left,
              ),
            ),
            SizedBox(height: size.height * 0.0),
            RoundedButton(
              backgroundColor: const Color(0xff33c0ba),
              text: "Next",
              //  backgroundColor: const Color(0xff33c0ba),
              // colors: [const Color(0xff145d7c), const Color(0xff33c0ba)],
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) {
                    return OTPPage();
                  }),
                );
              },
            ),
            SizedBox(height: size.height * 0.01),
            AlreadyKnowPasswordCheck(
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => WelcomeScreen(),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}

// // ignore: unused_element
// const String _svg_vygvi4 =
//     '<svg viewBox="600.8 1363.5 238.5 232.5" ><path transform="translate(600.77, 1325.89)" d="M 101.4104843139648 53.22090911865234 L 113.2283935546875 41.40298461914062 C 118.2323684692383 36.39900970458984 126.3239288330078 36.39900970458984 131.2746734619141 41.40298461914062 L 234.7612762451172 144.8363647460938 C 239.7652587890625 149.8403625488281 239.7652587890625 157.931884765625 234.7612762451172 162.8826293945312 L 131.2746734619141 266.3692016601562 C 126.2707061767578 271.3731994628906 118.1791458129883 271.3731994628906 113.228401184082 266.3692016601562 L 101.4104919433594 254.5513000488281 C 96.353271484375 249.4940795898438 96.45973968505859 241.2428283691406 101.6234283447266 236.2921142578125 L 165.7702026367188 175.1796569824219 L 12.77612400054932 175.1796569824219 C 5.696022033691406 175.1796569824219 0 169.4836273193359 0 162.4035186767578 L 0 145.3687133789062 C 0 138.2886047363281 5.696022033691406 132.5925750732422 12.77612400054932 132.5925750732422 L 165.7702026367188 132.5925750732422 L 101.623420715332 71.48011016845703 C 96.40650177001953 66.52935791015625 96.30003356933594 58.27812194824219 101.4104843139648 53.22089385986328 Z" fill="#ffffff" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';

// class Body extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     Size size = MediaQuery.of(context).size;
//     return Background(
//       child: SingleChildScrollView(
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: <Widget>[
//             Text(
//               "SIGNUP",
//               style: TextStyle(fontWeight: FontWeight.bold),
//             ),
//             SizedBox(height: size.height * 0.03),
//             // SvgPicture.asset(
//             //   "assets/icons/signup.svg",
//             //   height: size.height * 0.35,
//             // ),
//             RoundedInputField(
//               hintText: " Email",
//               onChanged: (value) {},
//             ),
//             // RoundedPasswordField(
//             //   onChanged: (value) {},
//             // ),
//             //RoundedButton(
//             //   text: "Login",
//             //   press: () {},
//             // ),
//             // SizedBox(height: size.height * 0.03),
//             AlreadyHaveAnAccountCheck(
//               login: false,
//               press: () {
//                 Navigator.push(
//                   context,
//                   MaterialPageRoute(
//                     builder: (context) {
//                       return RegisterScreen();
//                     },
//                   ),
//                 );
//               },
//             ),
//             OrDivider(),
//           ],
//         ),
//       ),
//     );
//   }
// }
